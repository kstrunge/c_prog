#include<stdio.h>
#include<math.h>
#include<complex.h>

int main() {
	double gam = tgamma(5);
	printf("gammafunction(5) = %g\n", gam);

	printf("j1 = %g\n",j1(0.5));
	printf("sqrt(-2) = (%g + %g*I)\n",creal(csqrt(-2)), cimag(csqrt(-2)));
	printf("e^i = (%g + %g*I)\n",creal(cexp(I)), cimag(cexp(I)));
	printf("e^(pi*i) = (%g + %g*I)\n",creal(cexp(I*M_PI)), cimag(cexp(I*M_PI)));
	printf("I^e = (%g + %g*I)\n\n",creal(cpow(I,M_E)), cimag(cpow(I,M_E)));
	
	long double x = 0.1111111111111111111111111111L;
	printf("Long double: %.25Lg\n",x);
	double y = 0.1111111111111111111111111111;
	printf("double: %.25lg\n",y);
	float z = 0.1111111111111111111111111111;
	printf("float: %.25g\n",z);
	return 0;
}
