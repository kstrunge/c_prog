#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#include<stdlib.h>

int ode_func(double x, const double u[], double dudx[], void* params) {
	dudx[0] = (2/sqrt(M_PI))*exp(-x*x);
return GSL_SUCCESS;
}

double myfunc(double x) {
	if (x<0) return -myfunc(-x);
	gsl_odeiv2_system sys;
	sys.function = ode_func;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	gsl_odeiv2_driver* driver;
	double hstart = 0.1, eps = 1e-5, abs = 1e-5;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45, hstart, abs, eps);
	
	double x0 = 0;
	double u[] = {0};
	
	gsl_odeiv2_driver_apply(driver, &x0, x, u);
	
	gsl_odeiv2_driver_free(driver);
	return u[0];	
}

int main(int argc, char** argv) {
	double ni, nf, nstep;
	if (argc>1){
		ni = atof(argv[1]);		
		nf = atof(argv[2]);		
		nstep = atof(argv[3]);		
	}
	else {
		ni = 0;
		nf = 3;
		nstep = 0.2;
	}
	for (double x=ni; x<nf; x+=nstep) {
		fprintf(stdout,"%g %g \n",x,myfunc(x));
	}
return 0;
}
