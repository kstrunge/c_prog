#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#include<math.h>

int ode_func(double t, const double y[], double dydt[], void *params)
{
	double epsilon=0.01;
	dydt[0] = y[1];
	dydt[1] = 1-y[0]+epsilon*y[0]*y[0];
	return GSL_SUCCESS;
}

double myfunc(double t){
	gsl_odeiv2_system sys;
	sys.function = ode_func;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = NULL;

	gsl_odeiv2_driver *driver;
	double hstart = 0.1, abs = 1e-5, eps = 1e-5;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys,
					       gsl_odeiv2_step_rkf45,
					       hstart, abs, eps);

	double t0 = 0;
	double y[2] = { 1 , -0.7 };
//	printf("y'erne er: %g and %g \n",y[0],y[1]);
	gsl_odeiv2_driver_apply(driver, &t0, t, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(){
	for (double x = 0; x < 196.22*M_PI; x += 0.1)
		fprintf(stderr, "%g %g\n", x, myfunc(x));
	
return 0;
}


