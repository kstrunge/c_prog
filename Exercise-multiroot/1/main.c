#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#define TYPE gsl_multiroot_fsolver_hybrids
#define EPS 1e-6

int root_equation
(const gsl_vector * x, void * params, gsl_vector * f)
{
	const double x0 = gsl_vector_get(x,0);
	const double x1 = gsl_vector_get(x,1);
	gsl_vector_set(f,0, 400*x0*x0*x0 + (2-400*x1)*x0 - 2);
	gsl_vector_set(f,1, 200*(x1-x0*x0));
return GSL_SUCCESS;
}

int main(int argc, char** argv){
	double start0 = 2;
	double start1 = 4;
	gsl_multiroot_function F;
	F.f=root_equation;
	F.n=2;
//	F.params=(void*)&xmax;

	gsl_multiroot_fsolver * S;
	S = gsl_multiroot_fsolver_alloc(TYPE,F.n);

	gsl_vector* start = gsl_vector_alloc(F.n);
	gsl_vector_set(start,0,start0);
	gsl_vector_set(start,1,start1);
//	printf("check start=%g %g\n",gsl_vector_get(start,0),gsl_vector_get(start,1));
	gsl_multiroot_fsolver_set(S,&F,start);

	fprintf(stderr, "%g %g\n", gsl_vector_get(S->x,0), gsl_vector_get(S->x,1));
	int flag, iter=0;
	do{
		iter+=1;
		fprintf(stderr, "%g %g\n", gsl_vector_get(S->x,0), gsl_vector_get(S->x,1));
		gsl_multiroot_fsolver_iterate(S);
		flag=gsl_multiroot_test_residual(S->f,EPS);
	}while(flag==GSL_CONTINUE);
	
	printf("NO iterations: %i\n", iter);
	double x0=gsl_vector_get(S->x,0);
	double x1=gsl_vector_get(S->x,1);

	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(start);

	printf("x=%g y=%g\n",x0,x1);
return 0;
}

