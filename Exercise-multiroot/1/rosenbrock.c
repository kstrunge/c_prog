#include<stdio.h>

double rosenbrock(double x, double y) {
        double f = (1-x)*(1-x) + 100 * (y-x*x)*(y-x*x);
return f;
}

int main() {
	double res = 0.1;
	for(double x=-2.0+res;x<=2.0+res;x+=res){
		for(double y=-2.0+res;y<=5.0+res;y+=res){
			printf("%g %g %g\n",x,y,rosenbrock(x,y));
		}
	}

return 0;
}
