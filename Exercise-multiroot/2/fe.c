#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<math.h>

int ode_wave(double r, const double y[], double dydx[], void *params)
{
	double e=*(double*)params;
	dydx[0] = y[1];
	dydx[1] = 2*(-1/r-e)*y[0];
	return GSL_SUCCESS;
}

double fe(double e,double rmax, int print)
{
	//	if(rmax<0)return fe(e,-rmax);
	double rmin = 0.01;
	if(rmax<rmin) {
		return rmax-rmax*rmax;
	}

	gsl_odeiv2_system sys;
	sys.function = ode_wave;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*)&e;

	gsl_odeiv2_driver *driver;
	double r = rmin;
	double rs = (rmax-rmin)/100; 
	double abs = 1e-12; 
	double eps = 1e-12;
	
	driver = gsl_odeiv2_driver_alloc_y_new(&sys,
					       gsl_odeiv2_step_rkf45,
					       rs, abs, eps);

	double y[] = { rmin-rmin*rmin, 1-2*rmin };
	
	for(double r0 = r; r0<rmax;  r0+=rs) {
	gsl_odeiv2_driver_apply(driver, &r, r0, y);
		if(print>0) {
		fprintf(stdout, "%g %g %g\n",r,y[0],r*exp(-r));
		}
	}
	gsl_odeiv2_driver_free(driver);
	return y[0];
}
