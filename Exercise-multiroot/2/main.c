#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#define TYPE gsl_multiroot_fsolver_hybrids
#define EPS 1e-6

double fe(double e,double rmax, int print);
double root(double eps0, double rmax);
int main() {
	double rmax = 8;
	double e = root(-1,rmax);
	fprintf(stderr,"lowest root = %g\n",e);
	fe(e,rmax,1);
return 0;
}


/*int root_equation
(const gsl_vector * r, void * params, gsl_vector * f)
{
	double rmax = *(double*)params;
	double e = gsl_vector_get(r,0);
	double mismatch = fe(e,rmax);
	gsl_vector_set(f,0,mismatch);
return GSL_SUCCESS;
}

int main(int argc, char** argv){
	double rmax = argc>1? atof(argv[1]):8;
	double estart = argc>2? atof(argv[2]):0;

	gsl_multiroot_function F;
	F.f=root_equation;
	F.n=1;
	F.params=(void*)&rmax;

	gsl_multiroot_fsolver * S;
	S = gsl_multiroot_fsolver_alloc(TYPE,F.n);

	gsl_vector* start = gsl_vector_alloc(F.n);
	gsl_vector_set(start,0,estart);
	gsl_multiroot_fsolver_set(S,&F,start);

	int flag; int iter = 0;
	do{
		iter += 1;
		gsl_multiroot_fsolver_iterate(S);
		flag=gsl_multiroot_test_residual(S->f,EPS);
		fprintf(stderr, "e=%g for iter=%i and rmax=%g\n", gsl_vector_get(S->x,0), iter, rmax);
	}while(flag==GSL_CONTINUE);

	double result=gsl_vector_get(S->x,0);
	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(start);
	fprintf(stderr,"e=%g\n",result);
	for(double r=0.1;r<=rmax;r+=0.1)
		printf("%g %g\n",r,fe(result,r));
return 0;
}*/
