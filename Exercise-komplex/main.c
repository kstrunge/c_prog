#include<stdio.h>
#include "komplex.h"
#include<math.h>

int main() {
	komplex c = komplex_new(1,2);
	komplex d = komplex_new(3,4);

	printf("komplex c is: ");	
	komplex_print(c);

	printf("komplex d is: ");	
	komplex_print(d);

	printf("Sum of c and d is: ");
	komplex_print(komplex_add(c,d));

	printf("Substraction of c from d is: ");
	komplex_print(komplex_sub(d,c));
	
	printf("c is now set to (-3 + 10*i): \n");
	printf("c is now: ");
	komplex_set(&c, -3.0, 10);
	komplex_print(c);
	return 0;
}
