#include<stdio.h>
#include "komplex.h"

void komplex_print (komplex z) {
printf("(%g + %g*i)\n",z.re,z.im);
}

komplex komplex_new(double x, double y) {
	komplex z;
	z.re = x;
	z.im =y;
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	z->re = x;
	z->im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re, a.im + b.im };
	return result;
}

komplex komplex_sub (komplex a, komplex b) {
	komplex result = { a.re - b.re, a.im - b.im };
	return result;
}

