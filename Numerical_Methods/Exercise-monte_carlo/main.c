#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include"mc.h"

double func(gsl_vector* xx) {
	assert(xx->size==1);
	double x = gsl_vector_get(xx,0);
	return sqrt(x);
}

double func_hard(gsl_vector* xx) {
	assert(xx->size==3);
	double x = gsl_vector_get(xx,0);
	double y = gsl_vector_get(xx,1);
	double z = gsl_vector_get(xx,2);
	return 1./(M_PI*M_PI*M_PI)/(1-cos(x)*cos(y)*cos(z));
}

int main() {
//Allocate variables
	int dim = 1,N;
	double Q_corr;
	gsl_vector* a = gsl_vector_alloc(dim);
	gsl_vector* b = gsl_vector_alloc(dim);
	gsl_vector* res = gsl_vector_alloc(2);

	gsl_vector_set(a,0,0);
	gsl_vector_set(b,0,1);
	N = 10000;
	plainmc(func,a,b,N,res);
	Q_corr = 2./3.;
	printf("Integral of sqrt(x) from 0 to 1 with %i Points\nExact Integral should be: %g\n",N,Q_corr);
	printf("Integral: %g\n",gsl_vector_get(res,0));
	printf("Estimated Error: %g\n",gsl_vector_get(res,1));
	printf("Actual Error: %g\n\n", fabs(Q_corr-gsl_vector_get(res,0)));

//Harder 3D integral
	dim = 3;
	gsl_vector* a2 = gsl_vector_alloc(dim);
	gsl_vector* b2 = gsl_vector_alloc(dim);
	gsl_vector* res2 = gsl_vector_alloc(2);

	gsl_vector_set(a2,0,0);
	gsl_vector_set(a2,1,0);
	gsl_vector_set(a2,2,0);
	gsl_vector_set(b2,0,M_PI);
	gsl_vector_set(b2,1,M_PI);
	gsl_vector_set(b2,2,M_PI);
	N = 100000;
	plainmc(func_hard,a2,b2,N,res2);
	Q_corr = 1.3932039296856768591842462603255;
	printf("Integral of 1/(1-cos(x)cos(y)cos(z)) from 0 to pi with %i Points\nExact Integral should be: %g\n",N,Q_corr);
	printf("Integral: %g\n",gsl_vector_get(res2,0));
	printf("Estimated Error: %g\n",gsl_vector_get(res2,1));
	printf("Actual Error: %g\n\n", fabs(Q_corr-gsl_vector_get(res2,0)));

	Q_corr = 2./3.;
	for (int i=10; i<1000; i+=10) {
//		plainmc(func_hard,a2,b2,i,res2);
//		fprintf(stderr,"%i %g %g\n",i, 
//					gsl_vector_get(res2,1), 
//					fabs(Q_corr-gsl_vector_get(res2,0))
//					);
		plainmc(func,a,b,i,res);
		fprintf(stderr,"%i %g %g\n",i, 
					gsl_vector_get(res,1), 
					fabs(Q_corr-gsl_vector_get(res,0))
					);
	}
//Free the memory
	gsl_vector_free(a);
	gsl_vector_free(b);
	gsl_vector_free(res);
	gsl_vector_free(a2);
	gsl_vector_free(b2);
	gsl_vector_free(res2);
return 0;
}
