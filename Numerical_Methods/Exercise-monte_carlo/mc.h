#ifndef HAVE_MC_H
#define HAVE_MC_H
void print_vector(gsl_vector*);
void plainmc(double f(gsl_vector*), gsl_vector* a, gsl_vector* b, int N, gsl_vector* res);

#endif
