#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>

void print_vector(gsl_vector* v) {
	for (int i=0; i<v->size;i++) {
		printf(" %g\n",gsl_vector_get(v,i));
	}
}


void plainmc(double f(gsl_vector*), gsl_vector* a, gsl_vector* b, int N, gsl_vector* res) {
	gsl_vector* rx = gsl_vector_alloc(a->size);
	void randomx (gsl_vector* a, gsl_vector* b, gsl_vector* rx) {
		for (int i=0; i<a->size; i++) {
			gsl_vector_set(rx,i, 
				       gsl_vector_get(a,i)+rand()/(1.0*RAND_MAX)*(gsl_vector_get(b,i)-gsl_vector_get(a,i)));
		}
	}
	double volume=1;
	for (int i=0; i<a->size;i++) {
		volume*=gsl_vector_get(b,i)-gsl_vector_get(a,i);
	}
	double sum=0, sum2=0;
	for (int i=0; i<N;i++){
		randomx(a,b,rx);
		double fx = f(rx);
		sum += fx;
		sum2 += fx*fx;
	}
	gsl_vector_free(rx);
	double mean=sum/N;
	double sigma = sqrt(sum2/N - mean*mean);
	double SIGMA = sigma/sqrt(N);
	
	gsl_vector_set(res,0,mean*volume);
	gsl_vector_set(res,1,SIGMA*volume);
}
