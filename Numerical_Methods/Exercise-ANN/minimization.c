#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<float.h>
#include<stdio.h>
#include"lin_eq.h"

void finite_diff(double f(const gsl_vector* x), gsl_vector* x, gsl_vector* df) {
	double eps = sqrt(DBL_EPSILON);
	double f_i = f(x);
	for (int i=0; i<x->size; i++) {
		double xi = gsl_vector_get(x,i);
		double dx =fabs(xi)*eps;
		if (fabs(xi)<sqrt(eps)) dx = eps;
		gsl_vector_set(x,i,xi+dx);
		gsl_vector_set(df, i, (f(x)-f_i)/dx);
		gsl_vector_set(x,i,xi);
//		fprintf(stderr,"gradient: %g %g\n",gsl_vector_get(df,0),gsl_vector_get(df,1));
	}
	}

int quasi_newton (double f(const gsl_vector* x),
			gsl_vector* x, 
			double epsilon,
			FILE* path_stream) {
	int n = x->size;
	double eps = sqrt(DBL_EPSILON);
	gsl_vector* df = 	gsl_vector_alloc(n);
	gsl_vector* df_next = 	gsl_vector_alloc(n);
	gsl_vector* ddf =	gsl_vector_alloc(n);
	gsl_vector* x_next = 	gsl_vector_alloc(n);
	gsl_vector* dx = 	gsl_vector_alloc(n);
	gsl_vector* u = 	gsl_vector_alloc(n);
	gsl_matrix* B = 	gsl_matrix_alloc(n,n);
	gsl_matrix_set_identity(B);
	int N_steps = 0, calls = 0;
	finite_diff(f,x,df); calls+=2;
	double fx=f(x);
	double f_next;
	do {
//itterative update of Hessian
//Newton step
		N_steps++;
		gsl_blas_dgemv(CblasNoTrans,-1,B,df,0,dx); //dx = -B*df newton step
		if (gsl_blas_dnrm2(dx)<eps*gsl_blas_dnrm2(x)) {
			fprintf(stderr,"Minimization converged due to step length below sqrt(DBL_EPSILON): %g\n",gsl_blas_dnrm2(dx));
			break;}
		if (gsl_blas_dnrm2(df)<epsilon) {
			fprintf(stderr,"Minimization converged due to gradient below user defined precision: %g\n",gsl_blas_dnrm2(df));
			break;}
//finding a good step by backtracking
		double lambda = 1;
		do {
			gsl_vector_memcpy(x_next,x);
			gsl_vector_add(x_next,dx);
			f_next = f(x_next); calls++;
			double dxTg;
			gsl_blas_ddot(dx, df, &dxTg);
			if (f_next<fx+0.1*dxTg) break;
			if (lambda<eps) { fprintf(stderr, "bad backtracking when N=%i\n", N_steps); break;} //bad break...
			gsl_vector_scale(dx,0.5);
			lambda *= 0.5;
		} while(1);
//Updating Hessian with Broyden's update dH = c*dxT
		finite_diff(f,x_next,df_next); calls+=2;
		gsl_vector_memcpy(ddf, df_next); //ddf: change in gradient
		gsl_blas_daxpy(-1,df,ddf); //ddf = df_next-df
		gsl_vector_memcpy(u, dx); //u: good backtracking step
		gsl_blas_dgemv(CblasNoTrans,-1,B,ddf,1,u); // u=dx-B*ddf
		double dxTddf, uTddf;
		gsl_blas_ddot(dx,ddf,&dxTddf);
		if (fabs(dxTddf)>1e-12) {
			gsl_blas_ddot(u,ddf,&uTddf);
			double k=uTddf/(2*dxTddf);
			gsl_blas_daxpy(-k,dx,u); //u = (dx-B*ddf) - dx*(uTddf/(2*dxTddf))
			gsl_blas_dger(1.0/dxTddf,u,dx,B); //symmetric rank-1 update
			gsl_blas_dger(1.0/dxTddf,dx,u,B); //H = 1/dxTddf*dx*u^T+B
		}
		for (int i=0; i<n; i++) {
			fprintf(path_stream,"%g ",gsl_vector_get(x,i));
		} fprintf(path_stream,"\n");
		gsl_vector_memcpy(x,x_next);
		gsl_vector_memcpy(df,df_next);
	} while(N_steps<200);
//#Free the Memory
	gsl_vector_free(df);
	gsl_vector_free(df_next);
	gsl_vector_free(ddf);
	gsl_vector_free(x_next);
	gsl_vector_free(dx);
	gsl_vector_free(u);
	gsl_matrix_free(B);
	return N_steps;
}


