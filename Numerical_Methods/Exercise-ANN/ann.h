#ifndef HAVE_ANN_H
#define HAVE_ANN_H

typedef struct {int n; double (*f)(double); gsl_vector* data;} ann;

ann* ann_alloc (int n_neurons, double(*activation_function)(double));
void ann_free(ann* network);
double feed_forward(ann* network, double x);
double mismatch(gsl_vector*, void* params);
void ann_train(ann* network, gsl_vector* x, gsl_vector* y);

double ann_derivative(ann* network, double x, double df(double));
double ann_anti_derivative(ann* network, double x, double a);
#endif
