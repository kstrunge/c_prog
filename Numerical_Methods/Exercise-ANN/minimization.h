#ifndef HAVE_MINIMIZATION_H
#define HAVE_MINIMIZATION_H


void finite_diff(double f(const gsl_vector* x), gsl_vector* x, gsl_vector* df);

int quasi_newton (double f(const gsl_vector* x), 
				gsl_vector* x, 
				double epsilon, 
				FILE* path_stream);
/*
int newton (void f(const gsl_vector* x, gsl_vector* fx),
				gsl_vector* x,
				double dx,
				double epsilon,
				FILE* path_stream);


*/
#endif

