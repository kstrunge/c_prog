#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"ann.h"
#include"vecmat.h"


//double f(double x) {return exp(-x*x);} //Gauss
//double f(double x) {double out = (atan(x)+0.5*M_PI)/M_PI; printf("out: %g\n",out); return out;}
//double f(double x) {return (atan(x)+0.5*M_PI)/M_PI;} //atan
//double f(double x) {return 1/(1+exp(-x));} //sigmoid
//double f(double x) {return tanh(x);} //hyperbolic tan
double f(double x) {if (x<0) {return 0;} return x;} //ReLu

int main(int argc, char** argv) {
// Reading trainning sets from "intput.txt" into a data_container
<<<<<<< HEAD
	int nn = 2 ;
=======
	int nn = 3;
>>>>>>> 1e6783aca54311558e7437a32382ac507576eea3
	int N_rows=7, N_cols=5, N_out=10;
	int N_lines = atoi(argv[1]);
	int N_patterns = N_lines/(N_rows+1);
	gsl_vector* results = gsl_vector_alloc(N_patterns);
	gsl_matrix* patterns = gsl_matrix_alloc(N_rows*N_patterns, N_cols);
//reading matrices from input
	double tmp_container;
	int pat_id=0, row_id=0;
	for (int line=0; line<N_lines; line++) {
		if (line%(N_rows+1)==0) {
			scanf("%lg\n",&tmp_container);
			gsl_vector_set(results, pat_id, tmp_container);
			pat_id++;
		}
		else {
			for (int i=0; i<N_cols; i++) {
//				printf("row: %i\n",row_id);
				scanf("%lg", &tmp_container);
				gsl_matrix_set(patterns,row_id,i,tmp_container);
			}
			scanf("\n");
			row_id++;
		}
	}

	ann* network = ann_alloc(nn, &f, N_rows*N_cols, N_out);
//Set initial parameters
//	for(int i=0;i<network->weights->size;i++){
//		double init_r = (1.0*rand())/RAND_MAX-0.5;
//		gsl_vector_set(network->weights,i,init_r);
//	}
//	for(int i=0;i<network->biases->size;i++){
//		double init_r = (1.0*rand())/RAND_MAX-0.5;
//		gsl_vector_set(network->biases,i,init_r);
//	}
//	for(int i=0;i<network->weights->size;i++){
//		double init_r = 2;
//		gsl_vector_set(network->weights,i,init_r);
//	}
//	for(int i=0;i<network->biases->size;i++){
//		double init_r = 2;
//		gsl_vector_set(network->biases,i,init_r);
//	}
/*	fprintf(stderr,"initial w:\n");
	print_vector(network->weights,stderr);
	fprintf(stderr,"initial b:\n");
	print_vector(network->weights,stderr);
*/	
	
	ann_train(network, results, patterns);
	FILE* params = fopen("trained_params","w");
	print_vector(network->weights, params);	
	print_vector(network->biases, params);	

// Free the memory!
	ann_free(network);
	gsl_vector_free(results);
	gsl_matrix_free(patterns);
return 0;
}
