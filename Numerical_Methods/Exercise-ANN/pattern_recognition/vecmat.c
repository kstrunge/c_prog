#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"vecmat.h"
#define FMT_mat 9.5
#define FMT_vec 9.3

void print_matrix (gsl_matrix* A, FILE* stream) {
        for (int i=0;i<A->size1;i++) {
                for (int j=0;j<A->size2;j++) {
                        fprintf(stream,"%g",gsl_matrix_get(A,i,j));
                }
        fprintf(stream,"\n");
        }
}
void print_vector (gsl_vector* v, FILE* stream) {
        for (int i=0;i<v->size;i++) {
                fprintf(stream,"%g\n",gsl_vector_get(v,i));
        }
}

void get_pattern (gsl_matrix* patterns, int i, gsl_matrix* pattern) {
	int row = pattern->size1;
	int col = pattern->size2;
	for (int r=0; r<row; r++) {
		for (int c=0; c<col; c++) {
			gsl_matrix_set(pattern,r,c,gsl_matrix_get(patterns,r+i*row, c));
		}
	}
}
