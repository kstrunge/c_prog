#ifndef HAVE_ANN_H
#define HAVE_ANN_H

typedef struct {int n; double (*f)(double); gsl_vector* weights; gsl_vector* biases; int n_in; int n_out;} ann;

void softmax(gsl_vector*, gsl_vector*);
void feed_forward_softmax(ann* network, gsl_vector* output, gsl_matrix* pattern);
ann* ann_alloc (int n_neurons, double(*activation_function)(double), int n_in, int n_out);
void ann_free(ann* network);
void feed_forward(ann* network, gsl_vector* output, gsl_matrix* pattern);
double mismatch(gsl_vector*, void* params);
void ann_train(ann* network, gsl_vector* results, gsl_matrix* patterns);

#endif
