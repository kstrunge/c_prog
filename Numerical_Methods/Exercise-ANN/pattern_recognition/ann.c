#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_multimin.h>
#include"vecmat.h"

typedef struct {int n_hid; double (*f)(double); gsl_vector* weights; gsl_vector* biases; int n_in; int n_out;} ann;

ann* ann_alloc (int n_hid, double(*activation_function)(double), int n_in, int n_out) {
	ann* network = malloc(sizeof(ann));
	network->n_hid=n_hid;
	network->n_in=n_in;
	network->n_out=n_out;
	network->f=activation_function;
	network->weights = gsl_vector_alloc((n_in+n_out)*n_hid);
	network->biases = gsl_vector_alloc((n_hid+n_out));
	return network;
}

void ann_restart(gsl_vector* v, double max_param, uint* seed) {
	fprintf(stderr,"Network reset with random parameters [%g,%g] and seed: %i\n",max_param, max_param, *seed);
	for(int i=0;i<v->size;i++){
		double init_r = max_param*2.0*rand_r(seed)/RAND_MAX-max_param;
		gsl_vector_set(v,i,init_r);
	}
}

void ann_free(ann* network) {
	gsl_vector_free(network->weights);
	gsl_vector_free(network->biases);
	free(network);
}

void softmax(gsl_vector* input, gsl_vector* output) {
	int N = output->size;
	double sum = 0;
	for (int i=0; i<N; i++) {
		sum += exp(gsl_vector_get(input,i));
	}
	for (int i=0; i<N; i++) {
		gsl_vector_set(output,i, exp(gsl_vector_get(input,i))/sum);
	}
}

void feed_forward_softmax(ann* network, gsl_vector* output, gsl_matrix* pattern) {
	int N_in = network->n_in;
	int N_hid = network->n_hid;
	int N_out = network->n_out;
	gsl_vector* softmax_in = gsl_vector_alloc(N_out);
	for (int k=0; k<N_out; k++) {
		double yk = 0;
		for (int j=0; j<N_hid; j++) {
			double zj = 0;
			for (int i=0; i<N_in; i++) {
// tranforming pattern matrix into input vector elements xi
				int mat_c = i%pattern->size2;
				int mat_r = (i-mat_c)/pattern->size2;
				double xi = gsl_matrix_get(pattern, mat_r, mat_c);
				double wij = gsl_vector_get(network->weights, j*N_in+i);
				zj += xi*wij;
			}
			double bj = gsl_vector_get(network->biases, j);
			zj = (network->f)(zj+bj);
			double ujk = gsl_vector_get(network->weights, N_in*N_hid+j+k*N_hid);
			yk += zj*ujk;
		}
		double bk = gsl_vector_get(network->biases, N_hid+k);
//		yk = (network->f)(yk+bk);
		gsl_vector_set(softmax_in, k, yk+bk);
	}
	softmax(softmax_in, output);
	gsl_vector_free(softmax_in);
}

void feed_forward(ann* network, gsl_vector* output, gsl_matrix* pattern) {
	int N_in = network->n_in;
	int N_hid = network->n_hid;
	int N_out = network->n_out;
	for (int k=0; k<N_out; k++) {
		double yk = 0;
		for (int j=0; j<N_hid; j++) {
			double zj = 0;
			for (int i=0; i<N_in; i++) {
// tranforming pattern matrix into input vector elements xi
				int mat_c = i%pattern->size2;
				int mat_r = (i-mat_c)/pattern->size2;
				double xi = gsl_matrix_get(pattern, mat_r, mat_c);
				double wij = gsl_vector_get(network->weights, j*N_in+i);
				zj += xi*wij;
			}
			double bj = gsl_vector_get(network->biases, j);
			zj = (network->f)(zj+bj);
			double ujk = gsl_vector_get(network->weights, N_in*N_hid+j+k*N_hid);
			yk += zj*ujk;
		}
		double bk = gsl_vector_get(network->biases, N_hid+k);
		yk = (network->f)(yk+bk);
		gsl_vector_set(output,k,yk);
	}
}


int is_equal(double a, double b, double eps) {
	if (a+eps>b && a-eps<b) return 1;
	else return 0;
}

void ann_train(ann* network, gsl_vector* result, gsl_matrix* patterns) {
//Minimize the mismatch function via the parameters in network->data
	int N_pat = result->size, N_out = network->n_out, N_in = network->n_in, N_hid = network->n_hid;
	int pat_width = patterns->size2, pat_height = patterns->size1/N_pat;
//allocating the needed vectors
	gsl_vector* output = gsl_vector_alloc(N_out);
	gsl_vector* correct = gsl_vector_alloc(N_out);
	gsl_matrix* pattern = gsl_matrix_alloc(pat_height, pat_width);
	int iter=0, iter_small=1;
	double accuracy, sum, dev, mismatch_result;
	int N_train = N_pat; //100;
//	int train_subset[N_train];
//	for (int i=0; i<N_train; i++) {
//		train_subset[i] = rand() % (N_pat);
//	}
	double mismatch(const gsl_vector* p, void *params) {
// Copy params in p into network (weights and biases)
		gsl_vector_const_view	viewp1 = gsl_vector_const_subvector( p, 0, network->weights->size);
		gsl_vector_const_view	viewp2 = gsl_vector_const_subvector( p, network->weights->size, network->biases->size);
		gsl_vector_memcpy( network->weights, &viewp1.vector);
		gsl_vector_memcpy( network->biases, &viewp2.vector);
		
// generate random subset for training		
//		if (iter%1200-1==0) {
//			for (int i=0; i<N_train; i++) {
//				train_subset[i] = rand() % (N_pat);
//			}
//		}
		accuracy = 0;
		sum = 0;
		int result_i;
		for (int subset=0; subset<N_train; subset++) {
			int i=subset;
//			int i = train_subset[subset];
//			fprintf(stderr,"i: %i\n",i);
			
			get_pattern(patterns, i , pattern);
			feed_forward_softmax(network,output,pattern);
			result_i = gsl_vector_get(result, i);
//			gsl_vector_set_all( correct, 1.0/(pow(iter_small+1,3/4)) );
			gsl_vector_set_all( correct, 2e-2 );
			gsl_vector_set(correct,result_i,1);
//			if (result_i+1<correct->size) gsl_vector_set(correct,result_i+1,1e-2);
//			if (result_i-1>-1) gsl_vector_set(correct,result_i-1,1e-2);
/*			fprintf(stderr,"correct:\n");
			print_vector(correct,stderr);
			fprintf(stderr,"output:\n");
			print_vector(output,stderr);
			fprintf(stderr,"\n");
*/			double sum_out = 0;
			for (int j=0; j<N_out; j++) {
				dev = gsl_vector_get(output,j) - gsl_vector_get(correct,j); // deviation
				sum_out += fabs(dev);
			}
			if (gsl_vector_max_index(output)==gsl_vector_get(result,i)) {accuracy += 100.0/N_train; }//fprintf(stderr,"succes accuracy have been raised to: %g\n",accuracy);}
			sum += sum_out;
			gsl_vector_set(correct,gsl_vector_get(result,i), 0);
		}
		mismatch_result = sum/N_train;
		if (iter%100==0) fprintf(stderr,"iter: %i mismatch: %g accuracy: %g %%\n", iter, mismatch_result,accuracy);
//		fprintf(stderr,"iter: %i mismatch: %g accuracy: %g %%\n", iter, mismatch_result,accuracy);
		return mismatch_result;
	}
// build param vector p of all parameters (weights and biases) in network 
	gsl_vector* p = gsl_vector_alloc((1+N_in+N_out)*N_hid+N_out);
	gsl_vector_view	viewp1 = gsl_vector_subvector( p, 0, network->weights->size);
	gsl_vector_view	viewp2 = gsl_vector_subvector( p, network->weights->size, network->biases->size);
	gsl_vector_memcpy( &viewp1.vector, network->weights );
	gsl_vector_memcpy( &viewp2.vector, network->biases );
//Minimization here
	int dim = network->biases->size + network->weights->size, dummy;
	assert(p->size == dim);

        gsl_multimin_function F;
        F.f = mismatch;
        F.n = dim;
        F.params = (void*)&dummy;
 
        gsl_multimin_fminimizer *M;
        #define TYPE gsl_multimin_fminimizer_nmsimplex2
        M = gsl_multimin_fminimizer_alloc(TYPE,dim);
        gsl_vector* step = gsl_vector_alloc(dim);
	gsl_vector_set_all(step, 0.3);

	uint seed = 1;
        int status, restarts = 0, iter_lim = 5000000;
        double size, mismatch_old, max_param;
	FILE* plot_out = fopen("plot.data","w");
	do {
		restarts++;
//		max_param = restarts*0.05+0.3;
		max_param = 1;
		ann_restart(p, max_param, &seed);
		gsl_multimin_fminimizer_set(M,&F,p,step);
		iter_small = 0;
	        do {
	                iter++;
			iter_small++;
			mismatch_old = mismatch_result;
	                status = gsl_multimin_fminimizer_iterate(M);
			if (status) {fprintf(stdout,"Unexpected break in minimizing\n"); break;} 
	
	 		if (is_equal(mismatch_result, mismatch_old, 1e-10) && restarts<50) {
				fprintf(stderr,"training stopped - Restart: %i iterations: %i accuracy reached: %g %%\n",restarts,iter_small,accuracy);
				fprintf(plot_out,"%i %i %g %g %g\n", iter, restarts, accuracy, mismatch_result, max_param);
				break;
			}
			if (iter_small>10000 && accuracy<30.0) {
				fprintf(stderr,"training stopped - Restart: %i iterations: %i accuracy reached: %g %% (slow convergence)\n",restarts,iter_small,accuracy);
				fprintf(plot_out,"%i %i %g %g %g\n", iter, restarts, accuracy, mismatch_result, max_param);
				break;
			}
			size = gsl_multimin_fminimizer_size(M);
	                status = gsl_multimin_test_size(size,1e-2);
			if (status == GSL_SUCCESS) {
				fprintf(stderr,"training succes  - Restart: %i iterations: %i accuracy reached: %g %%\n",restarts,iter_small,accuracy);//fprintf(stderr,"training succes - converged at minimum\n");
				fprintf(plot_out,"%i %i %g %g %g\n", iter, restarts, accuracy, mismatch_result, max_param);
				break;
			}
	 
		}while(status == GSL_CONTINUE && iter<iter_lim);
//		}while(iter<iter_lim);
		if (iter>iter_lim) break;
		mismatch_old = 0;
	}while(mismatch_result>0.85);

	printf("iter: %i\n",iter);
	printf("mismatch: %g\n",mismatch(M->x,(void*)&dummy));
	printf("training accuracy: %g %%\n", accuracy);
//	print_matrix(pattern,stderr);
//	feed_forward(network,output,pattern);
//	print_vector(output,stderr);

	fclose(plot_out);	
	gsl_vector_free(p);
	gsl_vector_free(step);
	gsl_vector_free(output);
	gsl_vector_free(correct);
	gsl_matrix_free(pattern);
}

