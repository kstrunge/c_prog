#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_matrix.h>
#include"ann.h"
#include"vecmat.h"

//double f(double x) {return exp(-x*x);} //Gauss
//double f(double x) {double out = (atan(x)+0.5*M_PI)/M_PI; printf("out: %g\n",out); return out;}
//double f(double x) {return (atan(x)+0.5*M_PI)/M_PI;} //atan
//double f(double x) {return 1/(1+exp(-x));} //sigmoid
//double f(double x) {return tanh(x);} //hyperbolic tan
double f(double x) {if (x<0) {return 0;} return x;} //ReLu

int main(int argc, char** argv) {
// Building neural network from saved params in text file
	int N_rows=7, N_cols=5, N_out=10;
	int N_in = N_rows*N_cols;
	int N_lines = atoi(argv[1]);
	int N_hid = (N_lines-N_out)/(1+N_in+N_out);
	ann* network = ann_alloc(N_hid, &f, N_in, N_out);
	double tmp;
	int N_weights = (N_in+N_out)*N_hid;
	for (int line=0; line<N_weights; line++) {
		scanf("%lg\n",&tmp);
		gsl_vector_set(network->weights, line, tmp);
	}
	int N_biases = (N_hid+N_out);
	for (int i=0; i<N_biases; i++) {
		scanf("%lg\n",&tmp);
		gsl_vector_set(network->biases, i, tmp);
	}
//	print_vector(network->data,stderr);

// Reading testing pattern from "test_pattern.txt"
	gsl_matrix* test_pattern = gsl_matrix_alloc(N_rows, N_cols);
	FILE* input_pat = fopen("test_pattern.txt","r");
	double tmp_container;
	int row = 0;
	for (int i=0; i<N_rows; i++) {
		for (int i=0; i<N_cols; i++) {
			fscanf(input_pat, "%lg", &tmp_container);
			gsl_matrix_set(test_pattern,row,i,tmp_container);
		}
		fscanf(input_pat, "\n");
		row++;
	}
//	print_matrix(test_pattern,stderr);
	
	gsl_vector* output = gsl_vector_alloc(N_out);
	feed_forward_softmax(network,output,test_pattern);
//	double norm = gsl_blas_dnrm2(output);
//	gsl_vector_scale(output,1.0/norm);
//	print_vector(output,stderr);
	for (int i=0; i<N_out; i++) {
		fprintf(stderr, "output%i: %g\n",i,gsl_vector_get(output,i));
	}
	
	fprintf(stderr,"\nThe input pattern:\n");
	print_matrix(test_pattern,stderr);

	int flag = 0;
	for (int i=0; i<N_out; i++) {
		if (gsl_vector_get(output,i)>0.65 && flag==0) {
			fprintf(stderr,"\nConclusion Reached!\nResult: %i\nConfidence: %g %%\n",i,gsl_vector_get(output,i)*100);
			flag++;	
		}
		else if (gsl_vector_get(output,i)>0.65 && flag>0) {
			fprintf(stderr,"Warning! - Additional possible result: %i\n",i);
		}
	}
	if (flag==0) { fprintf(stderr,"\nUnable to Reach Satisfying Conclusion.\n");}

	gsl_vector_free(output);
	gsl_matrix_free(test_pattern);
	ann_free(network);
	
return 0;
}

