#ifndef HAVE_MATRIX_H
#define HAVE_MATRIX_H

void print_matrix (gsl_matrix* A, FILE* stream);
void print_vector (gsl_vector* v, FILE* stream);
void get_pattern (gsl_matrix* patterns, int i, gsl_matrix* pattern);
#endif
