#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"ann.h"

double f(double x) { return exp(-x*x); }
double df(double x) { return -2*x*exp(-x*x); }
double func(double x) {return sin(x);}

int main() {
	int N = 10;
	int nn = 3;
	gsl_vector* x = gsl_vector_alloc(N);
	gsl_vector* y = gsl_vector_alloc(N);
	for (int i=0; i<N; i++) {
		double xx = i*10./N;
		double val = func(xx);
		printf("%g %g\n",xx, val);
		gsl_vector_set(x,i,xx);
		gsl_vector_set(y,i,val);
	}
	
	ann* network = ann_alloc(nn, *f);
	double xmin=gsl_vector_get(x,0);
	double xmax=gsl_vector_get(x,N-1);
	for(int i=0;i<nn;i++){
		gsl_vector_set(network->data,0+3*i,xmin+(i+0.5)*(xmax-xmin)/nn);
		gsl_vector_set(network->data,1+3*i,1);
		gsl_vector_set(network->data,2+3*i,0.5);
	}
	ann_train(network, x, y);
	
	for (double xx=xmin; xx<=xmax; xx+=(xmax-xmin)/50) {
		fprintf(stderr,"%g %g %g %g %g\n",xx, 
					feed_forward(network, xx), 
					func(xx), 
					ann_derivative(network,xx,df),
					ann_anti_derivative(network,xx,M_PI/2));
	} 	
// Free the memory!
	gsl_vector_free(x);
	gsl_vector_free(y);
	ann_free(network);
return 0;
}
