#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_multimin.h>
//#include"minimization.h"
//#include"lin_eq.h"

typedef struct {int n; double (*f)(double); gsl_vector* data;} ann;
struct data_container {gsl_vector* x; gsl_vector* y;};


ann* ann_alloc (int n_neurons, double(*activation_function)(double)) {
	ann* network = malloc(sizeof(ann));
	network->n=n_neurons;
	network->f=activation_function;
	network->data = gsl_vector_alloc(n_neurons*3);
	return network;
}

void ann_free(ann* network) {
	gsl_vector_free(network->data);
	free(network);
}

double feed_forward(ann* network, double x) {
	double sum = 0;
	for (int i=0; i<network->n; i++) {
		double ai = gsl_vector_get(network->data, 3*i);
		double bi = gsl_vector_get(network->data, 3*i+1);
		double wi = gsl_vector_get(network->data, 3*i+2);
		assert(bi!=0);
		double term = (network->f)((x-ai)/bi)*wi;
		sum += term;
	}
/*	printf("network data:\n");
	print_vector(network->data); */
	return sum;
}



void ann_train(ann* network, gsl_vector* x, gsl_vector* y) {
//Minimize the mismatch function via the parameters in network->data
	int N = x->size;
	double mismatch(const gsl_vector* p, void *params) {

		gsl_vector_memcpy(network->data,p);
		double sum = 0;
		for (int i=0; i<N; i++) {
			double err = feed_forward(network, gsl_vector_get(x,i)) - gsl_vector_get(y,i);
			sum += err*err;
		}
//		print_vector(p);
		return sum/N;
	}
	gsl_vector* p = gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p, network->data);
//Minimization here
	int dim = network->n*3, data;
	assert(p->size == dim);

        gsl_multimin_function F;
        F.f = mismatch;
        F.n = dim;
        F.params = (void*)&data;
 
        gsl_multimin_fminimizer *M;
        #define TYPE gsl_multimin_fminimizer_nmsimplex2
        M = gsl_multimin_fminimizer_alloc(TYPE,dim);
        gsl_vector* step = gsl_vector_alloc(dim);
	gsl_vector_set_all(step, 0.2);

        gsl_multimin_fminimizer_set(M,&F,p,step);
 
        int iter=0;
        int status;
        double size;
 
        do {
                iter++;
                status = gsl_multimin_fminimizer_iterate(M);
                if (status) {fprintf(stdout,"Unexpected break in minimizing\n"); break;}
 
                size = gsl_multimin_fminimizer_size(M);
                status = gsl_multimin_test_size(size,1e-2);
                if (status == GSL_SUCCESS) fprintf(stdout,"training succes - converged at minimum\n");
        }while(status == GSL_CONTINUE && iter<20000);
	
	gsl_vector_memcpy(network->data, M->x);
	printf("iter: %i\n",iter);
	printf("mismatch: %g\n",mismatch(network->data,(void*)&data));
	gsl_vector_free(p);
}

double ann_derivative(ann* network, double x, double df(double)) {
	double sum = 0;
	for (int i=0; i<network->n; i++) {
		double ai = gsl_vector_get(network->data, 3*i);
		double bi = gsl_vector_get(network->data, 3*i+1);
		double wi = gsl_vector_get(network->data, 3*i+2);
		double term = wi*df((x-ai)/bi)/bi;
		sum += term;
	}
	
return sum;
}



double ann_anti_derivative(ann* network, double x, double a) {
	double sum = 0;
	for (int i=0; i<network->n; i++) {
		double ai = gsl_vector_get(network->data, 3*i);
		double bi = gsl_vector_get(network->data, 3*i+1);
		double wi = gsl_vector_get(network->data, 3*i+2);
		double term = -wi*1/2*sqrt(M_PI)*bi* (erf((ai-x)/bi) - erf((ai-a)/bi));
		sum += term;
	}
	
return sum;
}

