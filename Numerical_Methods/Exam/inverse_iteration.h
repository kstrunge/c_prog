#ifndef HAVE_INV_ITER
#define HAVE_INV_ITER

int inverse_iteration();
void update_shift(gsl_matrix* A, gsl_matrix* Q, gsl_matrix* R, double lambda);
int not_equal (double a, double b);
int equal (double a, double b);

#endif
