#include<math.h>
#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"vecmat.h"
#include"inverse_iteration.h"
#include<gsl/gsl_eigen.h>


int main() {
/* Making formalities in "out.txt" */
	printf("____________________________________________\n\nPractical Programming and Numerical Methods\nName: Kris Strunge (201507033)\n");
	printf("(33 %% 23 = 10)\nExercise: 10. Inverse Iteration Algorithm \nfor Eigenvalues (and Eigenvectors)\nProblem Description: see 'report.pdf'\n____________________________________________\n\n");
// setting up test nxn matrix A and initial eigenvector x (random components in [-x_max,x_max]
	int n = 6;
	double x_max = 10;
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* A_copy = gsl_matrix_alloc(n,n);
// Test example from https://en.wikiversity.org/wiki/Shifted_inverse_iteration
/*	gsl_matrix_set(A,0,0,0);
	gsl_matrix_set(A,0,1,11);
	gsl_matrix_set(A,0,2,-5);
	gsl_matrix_set(A,1,0,-2);
	gsl_matrix_set(A,1,1,17);
	gsl_matrix_set(A,1,2,-7);
	gsl_matrix_set(A,2,0,-4);
	gsl_matrix_set(A,2,1,26);
	gsl_matrix_set(A,2,2,-10);*/
	for (int i=0; i<n; i++) {
		gsl_vector_set(x,i, 2.0*x_max*((double)rand()/RAND_MAX)-x_max);
		for (int j=0; j<n; j++) {
			gsl_matrix_set(A,i,j,2.0*x_max*((double)rand()/RAND_MAX)-x_max);
		}
	}
	double s = 1; //Shift s
	printf("Square Matrix A:\n");
	print_matrix(A,stdout);
	printf("\nInitial Eigenvector x_0:\n");
	print_vector(x,stdout);
	printf("\nLooking for eigenvalues around: s=%g\n", s);

//Do inverse iteration to find eigenvalue e and eigenvector v
	int iter;
	gsl_matrix_memcpy(A_copy,A);
	printf("\n***Running Inverse Iteration Algorithm***\n");
	iter = inverse_iteration(A_copy, x, &s); //s becomes eigenvalue and x becomes eigenvector
//print results and prove that v is eigenvector to A with corresponding eigenvalue e
	printf("Number of Iterations: %i\n",iter);
	printf("Eigenvalue: %g\nCorresponding Eigenvector:\n",s);
	print_vector(x,stdout);
	
	gsl_vector* Ax = gsl_vector_alloc(n);
	gsl_blas_dgemv(CblasNoTrans, 1, A, x, 0, Ax);
	printf("\nTest: Av == lambda*v\nAv=\n");
	print_vector(Ax,stdout);
	int test_flag = 1;
	for (int i=0;i<n;i++) {
		if (not_equal(gsl_vector_get(Ax,i), gsl_vector_get(x,i)*s)) test_flag = 0;
	}
	if (test_flag) printf("Test passed - Av == lambda*v <- hereby proved that we have found a eigenvalue and a eigenvector.\n");
	else printf("Test Failed - Av != lambda*v\n"); 

//investigate if algorithm converges with the expected scaling O(n^3) - see report.pdf figure.

//Comparing the eigenvalue with all eigenvalues calculated via "gsl_eigen.h"
	gsl_eigen_nonsymm_workspace* W = gsl_eigen_nonsymm_alloc(n);
	gsl_vector_complex* e = gsl_vector_complex_alloc(n);
	gsl_eigen_nonsymm(A,e,W);
	gsl_vector_view a = gsl_vector_complex_real(e);
	gsl_vector_view b = gsl_vector_complex_imag(e);
	printf("\nAll eigenvalues of matrix A from gsl_eigen.h via Schur Decomposition:\n");
	for (int i=0;i<n;i++) {
		printf("%g + %g*I",gsl_vector_get(&a.vector,i), gsl_vector_get(&b.vector,i));
		if (equal(gsl_vector_get(&a.vector,i),s)) printf(" <- this eigenvalue was found with inverse iteration");
		printf("\n");
	}

//Free the memory!
	gsl_vector_free(x);
	gsl_matrix_free(A);
	gsl_matrix_free(A_copy);
	gsl_eigen_nonsymm_free(W);
	gsl_vector_complex_free(e);
return 0;
}

