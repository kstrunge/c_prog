#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include"inverse_iteration.h"

int main(int argc, char** argv) {
// generate random nxn matrix A
        const int n = atoi(argv[1]);
        gsl_matrix* A = gsl_matrix_alloc(n,n);
        gsl_vector* x = gsl_vector_alloc(n);
	double s = 1;//(double)rand()/RAND_MAX;
        for (int i=0; i<n; i++) {
		gsl_vector_set(x,i, (double)rand()/RAND_MAX);
                for (int j=0; j<n; j++) {
                        double rand_double = (double)rand()/RAND_MAX;
                        gsl_matrix_set(A,i,j,rand_double);
                }
        }
        int iter = inverse_iteration(A,x,&s);
	printf("%i ",iter);
return 0;
}
