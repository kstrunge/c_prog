#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<time.h>



void qr_gs_decomp	(gsl_matrix* A, gsl_matrix* R) {
	int n = A->size1;
	int m = A->size2;
	double R_ii, R_ij, a_jk, a_ik;
	for(int i=0;i<m;i++) {
//making r_i
		R_ii = 0;
        	for(int k=0;k<n;k++) {
			a_ik = gsl_matrix_get(A,k,i);
        		R_ii += a_ik*a_ik;
		}
        	R_ii = sqrt(R_ii);
		gsl_matrix_set(R,i,i,R_ii);
		for(int k=0;k<n;k++) {
			gsl_matrix_set(A,k,i,gsl_matrix_get(A,k,i)/R_ii);
		}
		for(int j=i+1;j<m;j++) {
			R_ij = 0;
			for(int k=0;k<n;k++) {
				R_ij += gsl_matrix_get(A,k,i)*gsl_matrix_get(A,k,j);
			}
			gsl_matrix_set(R,i,j,R_ij);
			for(int k=0;k<n;k++) {
				a_jk = gsl_matrix_get(A,k,j)-gsl_matrix_get(A,k,i)*R_ij;
				gsl_matrix_set(A,k,j,a_jk);
			}
		}
	}
}
void qr_backsubs (gsl_vector* x, gsl_matrix* R) {
	int n = x->size;
	for(int i=n-1;i>=0;i--) {
		double s = gsl_vector_get(x,i);
		for(int j=i+1;j<n;j++) {
			s -= gsl_matrix_get(R,i,j)*gsl_vector_get(x,j);
		}
//		printf("s = %g\n",s);
		gsl_vector_set(x,i,s/gsl_matrix_get(R,i,i));
	}
}

void qr_gs_solve (gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x) {
	int n = Q->size1;
	// Apply QT to b
	double x_i;
	for(int i=0;i<n;i++) {
        	x_i = 0;
        	for(int j=0;j<n;j++) {
			x_i += gsl_matrix_get(Q,j,i)*gsl_vector_get(b,j);
		}
//		printf("x_i = %g\n",x_i);
		gsl_vector_set(x,i,x_i);
	}	
	qr_backsubs(x,R);
}

void qr_gs_inverse	(gsl_matrix* Q, gsl_matrix* R, gsl_matrix *B) {
// finding column in B b_i as the solution to A*b_i=e_i -> R*b_i = Q^T*e_i
	int n = Q->size1;
	for (int i=0; i<n ; i++) {
		gsl_vector* b_i = gsl_vector_alloc(n);
		gsl_vector* e_i = gsl_vector_calloc(n);
		
		for (int j=0; j<n; j++) {
			if (j==i) gsl_vector_set(e_i,j,1.0);
		}	
		qr_gs_solve(Q,R,e_i,b_i);
		for (int j=0; j<n; j++) {
		gsl_matrix_set(B,j,i, gsl_vector_get(b_i,j));
		}
		
		gsl_vector_free(b_i);	
		gsl_vector_free(e_i);		
	}
}




