#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"vecmat.h"
#define FMT_mat 9.5
#define FMT_vec 9.3

void print_matrix (gsl_matrix* A, FILE* stream) {
	for (int i=0;i<A->size1;i++) {
		for (int j=0;j<A->size2;j++) {
			printf(" %9.3g",gsl_matrix_get(A,i,j));
		}
	printf("\n");
	}
}
void print_vector (gsl_vector* v, FILE* stream) {
	for (int i=0;i<v->size;i++) {
		printf(" %9.5g\n",gsl_vector_get(v,i));
	}
}

