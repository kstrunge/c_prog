#include<math.h>
#include<stdio.h>
#include<assert.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"vecmat.h"
#include"qr.h"

void update_shift(gsl_matrix* A, gsl_matrix* Q, gsl_matrix* R, double lambda) {
	int n = A->size1;
	gsl_matrix_memcpy(Q,A);
	for (int i=0; i<n; i++) {
		gsl_matrix_set(Q,i,i, gsl_matrix_get(Q,i,i)-lambda);
	}
	qr_gs_decomp(Q,R);
}

int not_equal (double a, double b) {
	double e = 1e-13;
	double diff = fabs(a-b);
	if (diff<e) return 0;
	else return 1;
}
int equal (double a, double b) {
	double e = 1e-12;
	double diff = fabs(a-b);
	if (diff<e) return 1;
	else return 0;
}

int inverse_iteration(gsl_matrix* A, gsl_vector* x, double* s) {
// Build matrix (A-sI) and overwrite it into A
	assert(A->size1==A->size2);
	int n = A->size1;
	int N_iter = 0;

// Iteration starts:
	gsl_matrix* R = gsl_matrix_alloc(n,n); //additional allocation for QR factorization 
	gsl_matrix* Q = gsl_matrix_alloc(n,n); //<- This is not strictily necessary. if memory is a problem, matrix A can be used as Q and restored in each iteration.
	gsl_vector* x1 = gsl_vector_alloc(n); //this is x_{i+1}
	gsl_vector_memcpy(x1,x);
	gsl_matrix_memcpy(Q,A);
	update_shift(A,Q,R,*s);
	double c = 0, c_old;
	int update_freq = 1000;
	do {
		N_iter++;
//		printf("\nIteration: %i\n",N_iter);
		gsl_vector_memcpy(x,x1);
		c_old = c;
// Use QR solve (A-sI)x1 = x_i for x1=x_{i+1}
		qr_gs_solve(Q,R,x,x1);
// Calculate eigenvalue from eigenvector via Rayleigh quotient c(x_i) = x_{i+1}^T x_i /( x_i^T x_i )
		gsl_blas_ddot(x1, x, &c);
		double norm = gsl_blas_dnrm2(x);
		c = c/(norm*norm);
//re-scaling x_{i+1} for numerical stability (doesn't change eigenvalue or eigenvector)
		double abs_max = 0;
		for (int i=0; i<n; i++) {
			double abs_x2 = fabs(gsl_vector_get(x1,i));
			if (abs_x2>abs_max) abs_max = abs_x2;
		}
		gsl_vector_scale(x1, 1.0/abs_max); 
		if (N_iter%update_freq==2) {
			*s = 1.0/c + *s;
			update_shift(A,Q,R,*s);
		}
//		printf("x_{i+1}:\n");
//		print_vector(x1,stdout);
//		printf("c: %g\n",1.0/c);
// Converge if c(x_i) == c(x_{i+1}.
	}while(not_equal(c_old, c) && N_iter<100);

//calulate eigenvalue of A: lambda = 1/c + s
	*s = 1.0/c + *s;
//Storing final eigenvector x1 in x
	gsl_vector_memcpy(x,x1);
	
//Free the memory
	gsl_matrix_free(R);
	gsl_matrix_free(Q);
	gsl_vector_free(x1);

return N_iter;
}


