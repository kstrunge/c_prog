#ifndef HAVE_JACOBI
#define HAVE_JACOBI

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

#endif
