#include<stdio.h>
#include<stdlib.h>
#include<float.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include"lin_eq.h"
#include"newton.h"
#include<gsl/gsl_blas.h>

double him(double x, double y) {return (x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7);} 
double ros(double x, double y) {return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);}
 
int main(int argc, char** argv) {
// read initial gues from "init.dat"
	int n = atoi(argv[1]);
	double init[n];
	for (int i=0; i<n; i++) {
		scanf("%lg\n",&init[i]);
	}
	double init_ros[2] = {6,7};
	double init_him[2] = {2,3};

// setup initial vectors
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector* x_ros = gsl_vector_alloc(n);
	gsl_vector* x_him = gsl_vector_alloc(n);
	for (int i=0; i<n; i++) gsl_vector_set(x,i,init[i]);

// solving system of equation in 1)
	double eps = 1e-4;
	FILE* stdtest = fopen("test.path","w");
	int steps = newton_with_jacobian(test_J,x,eps,stdtest);
	fclose(stdtest);
	printf("Resulting Root of The system of Equation at:\n");
	printf("Number of Function Calls: %i\n",steps);
	print_vector(x); printf("\n");

//finding root of derivatives of rosenbrock
	for (int i=0; i<n; i++) gsl_vector_set(x,i,init_ros[i]);
	double ros_eps = DBL_EPSILON;
	FILE* stdros = fopen("rosenbrock.path","w");
	steps = newton_with_jacobian(rosenbrock_J,x,ros_eps,stdros);
	fclose(stdros);
	printf("Resulting Root of The Derivatives of the Rosenbrock Equation:\nFrom Wikipedia: f(a,a^2)=0 and a=1\n");
	printf("Number of Function Calls: %i\n",steps);
	print_vector(x);
	printf("The function value is %g\n",ros(gsl_vector_get(x,0), gsl_vector_get(x,1)));
	printf("\n");

//finding root of derivatives of himmelblau
	for (int i=0; i<n; i++) gsl_vector_set(x,i,init_him[i]);
	double him_eps = DBL_EPSILON;
	FILE* stdhim = fopen("himmelblau.path","w");
	steps = newton_with_jacobian(himmelblau_J,x,him_eps,stdhim);
	fclose(stdhim);
	printf("Resulting Root of The Derivatives of the Himmelblau Equation:\nFrom Wikipedia: f(3,2)=0, f(-2.805,3.131)=0, f(-3.779,-3.283)=0 and f(3.584,-1.848)=0.\n");
	printf("Number of Function Calls: %i\n",steps);
	print_vector(x);
	printf("The function value is %g\n",him(gsl_vector_get(x,0), gsl_vector_get(x,1)));
	printf("\n");


//Doing the same without the jacobian
	printf("\nNow the same Tests Without Analytic Jacobian, but with Finite Difference:\n");
	double dx = sqrt(DBL_EPSILON);
	
	for (int i=0; i<n; i++) gsl_vector_set(x,i,init[i]);
	FILE* stdtest2 = fopen("test2.path","w");
	steps = newton(test,x,dx,eps,stdtest2);
	fclose(stdtest2);
	printf("Resulting Root of The system of Equation at:\n");
	printf("Number of Function Calls: %i\n",steps);
	print_vector(x); printf("\n");

//finding root of derivatives of rosenbrock
	for (int i=0; i<n; i++) gsl_vector_set(x,i,init_ros[i]);	
	FILE* stdros2 = fopen("rosenbrock2.path","w");
	steps = newton(rosenbrock,x,dx,ros_eps,stdros2);
	fclose(stdros2);
	printf("Resulting Root of The Derivatives of the Rosenbrock Equation:\nFrom Wikipedia: f(a,a^2)=0 and a=1\n");
	printf("Number of Function Calls: %i\n",steps);
	print_vector(x); printf("\n");
	printf("The function value is %g\n",ros(gsl_vector_get(x,0), gsl_vector_get(x,1)));
	printf("\n");

//finding root of derivatives of himmelblau
	for (int i=0; i<n; i++) gsl_vector_set(x,i,init_him[i]);
	FILE* stdhim2 = fopen("himmelblau2.path","w");
	steps = newton(himmelblau,x,dx,him_eps,stdhim2);
	fclose(stdhim2);
	printf("Resulting Root of The Derivatives of the Himmelblau Equation:\nFrom Wikipedia: f(3,2)=0, f(-2.805,3.131)=0, f(-3.779,-3.283)=0 and f(3.584,-1.848)=0.\n");
	printf("Number of Function Calls: %i\n",steps);
	print_vector(x); 
	printf("The function value is %g\n",him(gsl_vector_get(x,0), gsl_vector_get(x,1)));
	printf("\n");
	


//rosenbrock contour plot
	double xbound = 10;
	double ybound = 50;
	FILE* ros_contour = fopen("contour_r.dat","w");
	for (double xx=-xbound; xx<xbound; xx+=xbound/100) {
		for (double yy=-ybound; yy<ybound; yy+=ybound/100) {
			double fxy = ros(xx,yy);
			fprintf(ros_contour,"%g %g %g\n",xx,yy,fxy);
		}
	}
	fclose(ros_contour);
//himmelblau contour plot
	double bound = 4;
	FILE* him_contour = fopen("contour_h.dat","w");
	for (double xx=-bound; xx<bound; xx+=bound/100) {
		for (double yy=-bound; yy<bound; yy+=bound/100) {
			double fxy = him(xx,yy);
			fprintf(him_contour,"%g %g %g\n",xx,yy,fxy);
		}
	}
	fclose(him_contour);
// Free the Memory!
	gsl_vector_free(x);
	gsl_vector_free(x_ros);
	gsl_vector_free(x_him);
return 0;
}

