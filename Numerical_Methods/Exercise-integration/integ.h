#ifndef HAVE_INTEG_H
#define HAVE_INTEG_H

double my_integrator(double f(double), 
			double a, 
			double b, 
			double acc, 
			double eps, 
			double *err);


double my_integrator_recursive(double f(double x),
			double a,
			double b,
			double acc,
			double eps,
			double* err,
			double int1,
			double int2);

double clenshaw_curtis(double f(double x), double a, double b, double acc, double eps, double* err);
double upper_inf(double f(double x), double a, double acc, double eps, double* err);
double lower_inf(double f(double x), double b, double acc, double eps, double* err);
double both_inf(double f(double x), double acc, double eps, double* err);

#endif
