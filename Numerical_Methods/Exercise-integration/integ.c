#include"integ.h"
#include<math.h>
#include<stdio.h>


double my_integrator(double f(double), 
		double a, 
		double b, 
		double acc, 
		double eps, 
		double *err) {
	if (isinf(a) && isinf(b)) return both_inf(f,acc,eps,err);
	if (isinf(b)) return upper_inf(f,a,acc,eps,err);
	if (isinf(a)) return lower_inf(f,b,acc,eps,err);
	double dx = (b-a);
	double int1 = f(a+dx*1/3);
	double int2 = f(a+dx*2/3);
	return my_integrator_recursive(f,a,b,acc,eps, err, int1, int2);
}

double my_integrator_recursive(double f(double x),
			double a,
			double b,
			double acc,
			double eps,
			double* err,
			double int1,
			double int2)
			{
	double dx = (b-a);
	double int3 = f(a+dx/6);
	double int4 = f(a+5*dx/6);
	double Q = (int1+int2+2*(int3+int4))/6*dx;
	double Qline = (int1+int2+int3+int4)/4*dx;
	double t = acc+eps*fabs(Q);
	*err = fabs(Q-Qline)/2;
	if (*err<t) {
//		fprintf(stderr,"%g %g %g\n",(b-a)/2,f((b-a)/2),Q);
		return Q;
	}
	else {
		double Q0 = my_integrator_recursive(f, a, (a+b)/2, acc/sqrt(2.), eps, err, int3, int1);
		double Q1 = my_integrator_recursive(f, (a+b)/2, b, acc/sqrt(2.), eps, err, int2, int4);
		return Q0+Q1;
	}
}



double clenshaw_curtis(double f(double x), double a, double b, double acc, double eps, double* err) {
	double dtheta = (b-a)/2;
	double g(double t) {
		return f((a+b)/2+dtheta*cos(t)) * sin(t)*dtheta;
	}
	return my_integrator(g, 0, M_PI, acc, eps, err);
}

double upper_inf(double f(double x), double a, double acc, double eps, double* err) {
	double g(double t) {
//		return f( a+t/(1-t) ) * 1/((1-t)*(1-t));
		return f( a+(1-t)/t ) * 1/(t*t);
	}
	for (double i=0; i<=1; i+=0.01) {
		fprintf(stderr, "%g %g\n", i, g(i));
	}
	return my_integrator(g, 0, 1, acc, eps, err); //1e-8, 0.9999999, acc, eps, err);
}

double lower_inf(double f(double x), double b, double acc, double eps, double* err) {
	double g(double t) {
		return f( b-(1-t)/t ) * 1/(t*t);
//		return f( b+t/(1+t) ) * 1/((1+t)*(1+t));
	}
	for (double i=0; i<=1; i+=0.001) {
		fprintf(stderr, "%g %g\n", i, g(i));
	}
	return my_integrator(g, 0, 1, acc, eps, err);
}

double both_inf(double f(double x), double acc, double eps, double* err) {
	double g(double t) {
//		return ( f( (1-t)/t ) + f(-(1-t)/t) )/(t*t);
		return f( t/(1-t*t) ) * (1+t*t)/((1-t*t)*(1-t*t));
	}
	for (double i=0; i<=1; i+=0.001) {
		fprintf(stderr, "%g %g\n", i, g(i));
	}
	return my_integrator(g, -1, 1, acc, eps, err);
}

