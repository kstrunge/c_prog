#include<gsl/gsl_integration.h>
#include"integ.h"
#include<math.h>
#include<stdio.h>


int main() {
	int calls = 0;
	double my_sqrt(double x) {
		calls++;
		return sqrt(x);
	}
	double a=0, b=1, acc=1e-4, eps=1e-4, err;
	long double Q;
	Q = my_integrator(my_sqrt, a, b, acc, eps, &err);
//	double Q_ref=0, xs=0.05, xe=1, f;
//	for (double x=0; x<xe; x+=xs) {
//		f = func(x+xs/2)*xs;
//		Q_ref += f;
//		fprintf(stderr,"%g %g %g\n", x, f, Q_ref); 
//	}
//	printf("Ref integral: %g\n",Q_ref);
	long double Q_correct = 2./3.;
	printf("Integral of sqrt(x) should be %Lg \nrequired absolute error: %g\nrequired relative error: %g\n",Q_correct,acc,eps);
	printf("Integral is: %Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error");
	printf("\n");
	
	calls = 0;
	double sqrt_inv(double x) {
		calls++;
		return 1/sqrt(x);
	}
	a=0, b=1, acc=1e-3, eps=1e-3;
	Q = my_integrator(sqrt_inv, a, b, acc, eps, &err);
	Q_correct = 2.;
	printf("Integral of 1/sqrt(x) should be %Lg \nrequired absolute error: %g\nrequired relative error: %g\n",Q_correct,acc,eps);
	printf("Integral is: %Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error\n");
	printf("\n");

	calls=0;
	printf("Clenshaw-Curtis Transformation:\n");
	a=0, b=1, acc=1e-3, eps=1e-3;
	Q = clenshaw_curtis(sqrt_inv, a, b, acc, eps, &err);
	Q_correct = 2.;
	printf("Integral of 1/sqrt(x) should be %Lg \nrequired absolute error: %g\nrequired relative error: %g\n",Q_correct,acc,eps);
	printf("Integral is: %Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error\n");
	printf("\n");
	
	calls = 0;
	double ln_per_sqrt(double x) {
		calls++;
		return log(x)/sqrt(x);
	}
	a=0, b=1, acc=1e-3, eps=1e-3;
	Q = my_integrator(ln_per_sqrt, a, b, acc, eps, &err);
	Q_correct = -4.;
	printf("Integral of ln(x)/sqrt(x) should be %Lg \nrequired absolute error: %g\nrequired relative error: %g\n",Q_correct,acc,eps);
	printf("Integral is: %Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error\n");
	printf("\n");

	calls=0;
	a=0, b=1, acc=1e-3, eps=1e-3;
	Q = clenshaw_curtis(ln_per_sqrt, a, b, acc, eps, &err);
	Q_correct = -4.;
	printf("Clenshaw-Curtis Transformation:\n");
	printf("Integral of ln(x)/sqrt(x) should be %Lg \nrequired absolute error: %g\nrequired relative error: %g\n",Q_correct,acc,eps);
	printf("Integral is: %Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error\n");
	printf("\n");
	
	calls = 0;
	double pi(double x) {
		calls++;
		return 4*sqrt(1-(1-x)*(1-x));
	}
	a=0, b=1, acc=1e-12, eps=1e-12;
	Q = my_integrator(pi, a, b, acc, eps, &err);
	Q_correct = M_PI;
	printf("Integral of 4*sqrt(1-(1-x)^2) should be %.80Lg \nrequired absolute error: %g\nrequired relative error: %g\n",Q_correct,acc,eps);
	printf("Integral is: %.80Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error\n");
	printf("\n");
	
	calls = 0;
	a=0, b=1, acc=1e-12, eps=1e-12;
	Q = clenshaw_curtis(pi, a, b, acc, eps, &err);
	Q_correct = M_PI;
	printf("Clenshaw-Curtis Transformation:\n");
	printf("Integral of 4*sqrt(1-(1-x)^2) should be %.80Lg \nrequired absolute error: %g\nrequired relative error: %g\n",Q_correct,acc,eps);
	printf("Integral is: %.80Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error\n");
	printf("\n");

	printf("\n--- Testing Infinties ---\n");
	calls = 0;
	double upper_inf(double x) {
		calls++;
		return 1/(x*x);
//		return 1/((x+1)*sqrt(x)); // a=1 should give pi. result: 3.1405
	}
	a=1, b=INFINITY, acc=0.001, eps=0.001;
	Q=my_integrator(upper_inf, a, b, acc, eps, &err);
	Q_correct = 1;
	printf("Upper Infinte Integral of 1/x^2 in [%g;%g] should be %Lg \nrequired absolute error: %g\nrequired relative error: %g\n",a,b,Q_correct,acc,eps);
	printf("Integral is: %Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error\n");
	
	gsl_integration_workspace* w = gsl_integration_workspace_alloc(1000);
	double upper_inf_gsl(double x, void* params) {
		return upper_inf(x);
	}
	int dummy;
	double Q_gsl;
	gsl_function F;
	F.function = &upper_inf_gsl;
	F.params = &dummy;
	calls = 0;
	gsl_integration_qagiu(&F,a,acc,eps,1000,w,&Q_gsl,&err);
	printf("With gsl_integration_qags:\n Q=%g\n error=%g\n calls: %i\n\n",Q_gsl,err,calls);
	
	calls = 0;
	double lower_inf(double x) {
		calls++;
//		return sin(x)/x;
		return x*exp(-x*x);
	}
	a=-INFINITY, b=0, acc=0.001, eps=0.001;
	Q=my_integrator(lower_inf, a, b, acc, eps, &err);
	Q_correct = -1.0/2.0;
	printf("Lower Infinte Integral of x*exp(-x^2) in [%g;%g] should be %Lg \nrequired absolute error: %g\nrequired relative error: %g\n",a,b,Q_correct,acc,eps);
	printf("Integral is: %Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error\n");
	
	double lower_inf_gsl(double x, void* params) {
		return lower_inf(x);
	}
	F.function = &lower_inf_gsl;
	F.params = &dummy;
	calls = 0;
	gsl_integration_qagil(&F,b,acc,eps,1000,w,&Q_gsl,&err);
	printf("With gsl_integration_qags:\n Q=%g\n error=%g\n calls: %i\n\n",Q_gsl,err,calls);

	calls = 0;
	double both_inf(double x) {
		calls++;
		return exp(-x*x);
	}
	a=-INFINITY, b=INFINITY, acc=0.001, eps=0.001;
	Q=my_integrator(both_inf, a, b, acc, eps, &err);
	Q_correct = sqrt(M_PI);
	printf("Infinte Integral of exp(-x^2) in [%g;%g] should be %Lg \nrequired absolute error: %g\nrequired relative error: %g\n",a,b,Q_correct,acc,eps);
	printf("Integral is: %Lg\nError estimated to: %g\n", Q, err);
	printf("Function was called %i times\n",calls);
	if (fabs(Q_correct-Q)<err && err<acc) printf("Test Passed - Actual error < estimated error < required error\n");

	double both_inf_gsl(double x, void* params) {
		return both_inf(x);
	}
	F.function = &both_inf_gsl;
	F.params = &dummy;
	calls = 0;
	gsl_integration_qagi(&F,acc,eps,1000,w,&Q_gsl,&err);
	printf("With gsl_integration_qags:\n Q=%g\n error=%g\n calls: %i\n\n",Q_gsl,err,calls);


return 0;
}

