#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include"jacobi.h"

int main(int argc, char** argv) {
// generate random nxm symetric matrix
        const int n = atoi(argv[1]);
        gsl_matrix* A = gsl_matrix_alloc(n,n);
        gsl_matrix* V = gsl_matrix_alloc(n,n);
        gsl_vector* e = gsl_vector_alloc(n);

        for (int i=0; i<n; i++) {
                for (int j=0; j<=i; j++) {
                        double rand_double = (double)rand()/RAND_MAX;
                        gsl_matrix_set(A,i,j,rand_double);
                        gsl_matrix_set(A,j,i,rand_double);
                }
        }
//	print_matrix(A);
/* Jacobi method by iterative cyclic sweeps to generate eigenvalues (in vector)
and eigenvectors (in matrix) such that A=VDV^T*/
        int sweeps = jacobi(A,e,V);
	printf("%i\n",sweeps);
return 0;
}
