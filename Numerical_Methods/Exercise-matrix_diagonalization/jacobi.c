#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<math.h>


void print_matrix (gsl_matrix* A) {
	for (int i=0;i<A->size1;i++) {
		for (int j=0;j<A->size2;j++) {
			printf(" %9.3g",gsl_matrix_get(A,i,j));
		}
	printf("\n");
	}
}
void print_vector (gsl_vector* v) {
	for (int i=0;i<v->size;i++) {
		printf(" %9.5g\n",gsl_vector_get(v,i));
	}
}


int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V) {
// initializing vec and mat
	int n = A->size1;
	int sweep = 0;
	for (int i=0; i<n; i++) {
		gsl_vector_set(e, i, gsl_matrix_get(A,i,i));
		for (int j=0; j<n; j++) {
			if (i==j) gsl_matrix_set(V,i,j,1.0);
			else gsl_matrix_set(V,i,j,0.0);
		}
	}
// Iterative Jacobi sweeps until diagonal values doesn't change (flag=0)
	int flag = 1;
	while (flag) {
		flag = 0;
		sweep++;
		int p,q;
		for (q=0;q<n;q++) {
			for (p=0;p<q;p++) {
				double pp = gsl_vector_get(e,p);
				double qq = gsl_vector_get(e,q);
				double pq = gsl_matrix_get(A,p,q);
				double phi = 0.5*atan2(2.0*pq,qq-pp);
				double c = cos(phi);
				double s = sin(phi);
				double pp_next = c*c*pp-2.0*s*c*pq+s*s*qq;
				double qq_next = s*s*pp+2.0*s*c*pq+c*c*qq;
//				printf("p = %i, q = %i \n",p,q);
//				printf("qq = %g, qq_n = %g \n",qq,qq_next);
//				printf("pp = %g, pp_n = %g \n",pp,pp_next); printf("\n");
				if (pp!=pp_next || qq!=qq_next) {
					// Eigenvalues have changed
					flag = 1;
					gsl_vector_set(e,p,pp_next);
					gsl_vector_set(e,q,qq_next);
					gsl_matrix_set(A,p,q,0.0);
					for(int i=0;i<p;i++){
						double ip=gsl_matrix_get(A,i,p);
						double iq=gsl_matrix_get(A,i,q);
						gsl_matrix_set(A,i,p,c*ip-s*iq);
						gsl_matrix_set(A,i,q,c*iq+s*ip); 
					}
					for(int i=p+1;i<q;i++){
						double pi=gsl_matrix_get(A,p,i);
						double iq=gsl_matrix_get(A,i,q);
						gsl_matrix_set(A,p,i,c*pi-s*iq);
						gsl_matrix_set(A,i,q,c*iq+s*pi); 
					}
					for(int i=q+1;i<n;i++){
						double pi=gsl_matrix_get(A,p,i);
						double qi=gsl_matrix_get(A,q,i);
						gsl_matrix_set(A,p,i,c*pi-s*qi);
						gsl_matrix_set(A,q,i,c*qi+s*pi); 
					}	
					for(int i=0;i<n;i++){
						double vip=gsl_matrix_get(V,i,p);
						double viq=gsl_matrix_get(V,i,q);
						gsl_matrix_set(V,i,p,c*vip-s*viq);
						gsl_matrix_set(V,i,q,c*viq+s*vip); 
					}
				}
			}
		}
//	print_matrix(A); printf("\n");
//	print_vector(e); printf("\n");
	}
return sweep;
}




