#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

int jacobi_ebe(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int ne);
