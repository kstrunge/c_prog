#ifndef HAVE_JACOBI_CLASSIC
#define HAVE_JACOBI_CLASSIC

int jacobi_classic(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

#endif
