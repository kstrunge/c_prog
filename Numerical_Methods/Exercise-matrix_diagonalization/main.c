#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"jacobi.h"
#include"jacobi_ebe.h"
#include"jacobi_classic.h"
#include<stdlib.h>
#include<gsl/gsl_blas.h>
#define DIM 5

int main(int argc, char** argv) {
// generate random nxm symetric matrix
	const int n = DIM;
	uint seed = 1;
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	gsl_vector* e = gsl_vector_alloc(n);
	gsl_matrix* D = gsl_matrix_calloc(n,n);
	gsl_matrix* A_copy = gsl_matrix_alloc(n,n);
	gsl_matrix* V_copy = gsl_matrix_alloc(n,n);

	for (int i=0; i<n; i++) {
		for (int j=0; j<=i; j++) {
			double rand_double = (double)rand_r(&seed)/RAND_MAX;
			gsl_matrix_set(A,i,j,rand_double);
			gsl_matrix_set(A,j,i,rand_double);
		}
	}
	printf("Random symmetric matrix A:\n");
	print_matrix(A); printf("\n");
	gsl_matrix_memcpy(A_copy,A);

/* Jacobi method by iterative cyclic sweeps to generate eigenvalues (in vector)
and eigenvectors (in matrix) such that A=VDV^T*/
	int sweeps = jacobi(A,e,V);
	printf("No of Jacobi sweeps: %i\n",sweeps);
	printf("No of Jacobi Rotations: %i\n",sweeps*(n*n-n)/2);
	printf("Vector of Eigenvalue:\n");
	print_vector(e); printf("\n");
	printf("Matrix V of Eigenvectors:\n");
	print_matrix(V); printf("\n");
	printf("New Matrix A:\n");
	print_matrix(A); printf("\n");
	for (int i=0; i<n; i++) {
		gsl_matrix_set(D,i,i,gsl_vector_get(e,i));
//		for (int j=0; j<i; j++) gsl_matrix_set(D,i,j,gsl_matrix_get(A,j,i));
	}	
		
	printf("Diagonal Matrix D:\n");
	print_matrix(D); printf("\n");

// check that V^TAV=D
	gsl_matrix_memcpy(V_copy,V);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A_copy,V,0.0,D);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V_copy,D,0.0,A_copy);

	printf("Matrix V^TAV=D:\n");
	print_matrix(A_copy); printf("\n");
	

//check that scalling of jacobi algorithm is O(n^3)
// see plot.svg

// B: Jacobi e-by-e
// Restoring matrix A	
	for (int i=0; i<n; i++) {
		for (int j=0; j<=i; j++) {
			gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
		}
	}
//	print_matrix(A);
	int n_eigen = 1;
	int sweeps_ebe = jacobi_ebe(A,e,V,n_eigen);
	printf("Finding %i Eigenvalue(s) with %i single row sweeps\n",n_eigen,sweeps_ebe);
	printf("No of Jacobi Rotations: %i\n",sweeps_ebe*(n-1));
	for (int i=0; i<n_eigen; i++) printf("%9.5g\n",gsl_vector_get(e,i)); 
	printf("\n");
//	print_matrix(A); printf("\n");

// Restoring matrix A	
	for (int i=0; i<n; i++) {
		for (int j=0; j<=i; j++) {
			gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
		}
	}
	printf("Random symmetric matrix A:\n");
	print_matrix(A); printf("\n");
	gsl_matrix_memcpy(A_copy,A);
/* Classic Jacobi method by sweeps eliminating the higest off-diagonal elements 
 to generate eigenvalues (in vector) and eigenvectors (in matrix) such that A=VDV^T*/
	sweeps = jacobi_classic(A,e,V);
	printf("No of sweeps: %i\n",sweeps);
	printf("No of Jacobi Rotations: %i\n",sweeps*n);
	printf("Vector of Eigenvalue:\n");
	print_vector(e); printf("\n");
	printf("Matrix V of Eigenvectors:\n");
	print_matrix(V); printf("\n");
	printf("New Matrix A:\n");
	print_matrix(A); printf("\n");
	for (int i=0; i<n; i++) {
		gsl_matrix_set(D,i,i,gsl_vector_get(e,i));
//		for (int j=0; j<i; j++) gsl_matrix_set(D,i,j,gsl_matrix_get(A,j,i));
	}	
		
	printf("Diagonal Matrix D:\n");
	print_matrix(D); printf("\n");

// check that V^TAV=D
	gsl_matrix_memcpy(V_copy,V);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A_copy,V,0.0,D);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V_copy,D,0.0,A_copy);

	printf("Matrix V^TAV=D:\n");
	print_matrix(A_copy); printf("\n");

	gsl_matrix_free(A);	
	gsl_matrix_free(V);	
	gsl_vector_free(e);	
	gsl_matrix_free(D);	
	gsl_matrix_free(A_copy);	
	gsl_matrix_free(V_copy);	
return 0;
}
