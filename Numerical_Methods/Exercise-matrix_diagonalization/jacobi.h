#ifndef HAVE_JACOBI
#define HAVE_JACOBI

void print_matrix(gsl_matrix* A);
void print_vector(gsl_vector* v);
int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

#endif
