#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include"qspline.h"

struct data{
	int n;
	double x[50]; 
	double y[50];
	};

int main(int argc, char** argv) {
/* Read Input */
	struct data points;
	points.n = atoi(argv[1]);
	for (int i=0; i<points.n; i++) {
		scanf("%lg %lg\n",&points.x[i], &points.y[i]);
	}
	
	qspline *s = qspline_alloc(points.n, points.x, points.y);
	
	for (double x=points.x[0]; x<points.x[points.n-1]; x+=0.2) {
		printf("%g %g %g %g\n", x, qspline_evaluate(s, x), qspline_integral(s, x), qspline_derivative(s,x));
	}
	qspline_free(s);
	
return 0;
}

