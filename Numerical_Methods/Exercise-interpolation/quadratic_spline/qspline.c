#include<stdlib.h>
#include<math.h>
#include<stdio.h>
#include"qspline.h"
#include<assert.h>


double anti_deriv(qspline* s, int i, double z) {
// extra function for integral
	double k = 0;
	return k + s->y[i]*z + 0.5*s->b[i]*z*z - s->b[i]*s->x[i]*z + 
		s->c[i]*(z*z*z/3.0 + s->x[i]*s->x[i]*z - s->x[i]*z*z);
}

int binary_search (qspline *s, double z) {
	// Binary Search - Returns first index in interval containing z
        int i = 0, j = s->n-1;
        while(j-i>1) {
                int mid = (i+j)/2;
                if (z >= s->x[mid]) i = mid;
                else j = mid;
        }
	return i;
}
	
qspline * qspline_alloc(int n, double *x, double *y) {
	assert(n >= 0);
	qspline *s = (qspline *)malloc(sizeof(qspline));
	s->n = n;
	s->x = x;
	s->y = y;
	s->b = (double *) malloc(sizeof(double)*(n+1));
	s->c = (double *) malloc(sizeof(double)*(n+1));
// assume linear derivative in the beginning 
	s->b[0] = (s->y[1] - s->y[0])/(s->x[1] - s->x[0]);
	for (int i=0; i<n; i++) {
		s->c[i] = (s->y[i+1] - s->y[i] - s->b[i]*(s->x[i+1] - s->x[i])) /
				( (s->x[i+1] - s->x[i])*(s->x[i+1] - s->x[i]) );
		s->b[i+1] = s->b[i] + 2*s->c[i]*(s->x[i+1]-s->x[i]);
	}
	return s;
}

void qspline_free(qspline *s) {
	free(s->b);
	free(s->c);

}

double qspline_evaluate (qspline *s, double z) {
	int i = binary_search(s,z);
	double spline = s->y[i] + s->b[i]*(z - s->x[i]) + s->c[i]*(z-s->x[i])*(z-s->x[i]);
	return spline;
}

double qspline_derivative (qspline *s, double z) {
	int i = binary_search(s,z);
	double spline_deriv = s->b[i] + 2*s->c[i]*(z-s->x[i]);	
	return spline_deriv;
}

double qspline_integral (qspline *s, double z) {
	int i = binary_search(s,z);
	double spline = 0;
	for(int l=0; l<i; l++) {
		spline += 0;
		spline += anti_deriv(s,l,s->x[l+1]) - anti_deriv(s,l,s->x[l]);
        }
	spline += anti_deriv(s,i,z) - anti_deriv(s,i,s->x[i]);
	return spline;
}



