#include<math.h>
#include<stdio.h>

double linterp (int n, double *x, double *y, double z) {
//	Binary Search
	int i = 0, j = n-1;
	while(j-i>1) {
		int mid = (i+j)/2;
		if (z >= x[mid]) i = mid;
		else j = mid;
	}
	double b_i = (y[j]-y[i])/(x[j]-x[i]);
	double a_i = y[i];
	double spline = a_i + b_i * (z-x[i]);	
return spline;
}

double linterp_integ (int n, double *x, double *y, double z) {
	//      Binary Search
        int i = 0, j = n-1;
        while(j-i>1) {
                int mid = (i+j)/2;
                if (z >= x[mid]) i = mid;
                else j = mid;
        }
	double spline_int = 0;
	for(int l=0; l<i; l++) {
		double b = (y[l+1]-y[l])/(x[l+1]-x[l]);
		double a = y[l];
		spline_int += a*x[l+1] + b*0.5*x[l+1]*x[l+1] - b*x[l]*x[l+1] - (a*x[l] - b*x[l]*x[l] + 0.5*b*x[l]*x[l]);
	}

	double b = (y[j]-y[i])/(x[j]-x[i]);
	double a = y[i];
	//z = z-x[i];
	spline_int += a*z + b*0.5*z*z - b*x[i]*z - (a*x[i] - b*x[i]*x[i] + 0.5*b*x[i]*x[i]);
	spline_int -= 1;
	return spline_int;
}
