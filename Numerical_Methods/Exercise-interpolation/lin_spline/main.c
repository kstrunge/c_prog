#include<math.h>
#include<stdlib.h>
#include<stdio.h>

struct cspline{
	int n;
	double x[101]; 
	double y[101];
	};

double linterp(int n, double *x, double *y, double z);
double linterp_integ(int n, double *x, double *y, double z);

int main(int argc, char** argv) {
/* Read Input */
	struct cspline points;
	points.n = atoi(argv[1]);
	for (int i=0; i<points.n; i++) {
		scanf("%lg %lg\n",&points.x[i], &points.y[i]);
	}

/* Call linear interpolation function and integration */
	for (double x=points.x[0]; x <= points.x[points.n-1]; x+=0.2) {
		double spline = linterp(points.n, points.x, points.y, x);
		double spline_int = linterp_integ(points.n, points.x, points.y, x);
		printf("%g %g %g\n",x,spline, spline_int);
	}
return 0;
}

