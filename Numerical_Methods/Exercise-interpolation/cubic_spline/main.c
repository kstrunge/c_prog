#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_spline.h>

struct data{
	int n;
	double x[50]; 
	double y[50];
	};

int main(int argc, char** argv) {
/* Read Input */
	struct data points;
	points.n = atoi(argv[1]);
	for (int i=0; i<points.n; i++) {
		scanf("%lg %lg\n",&points.x[i], &points.y[i]);
	}
	
	gsl_interp_accel* acc = gsl_interp_accel_alloc ();
	gsl_spline* spline = gsl_spline_alloc (gsl_interp_cspline, points.n);
	
	gsl_spline_init (spline, points.x, points.y, points.n);
	
	double y;
	for (double x=points.x[0]; x<points.x[points.n-1]; x+=0.02) {
		y = gsl_spline_eval (spline, x, acc);
		printf("%g %g\n", x, y);
	}
	gsl_spline_free (spline);
	gsl_interp_accel_free (acc);
	
return 0;
}

