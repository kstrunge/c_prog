#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<float.h>
#include<stdio.h>
#include"lin_eq.h"

int newton (double f(const gsl_vector* x, gsl_vector* df, gsl_matrix* H), 
				gsl_vector* x, 
				double epsilon,
				FILE* path_stream) {
	int n = x->size;
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* df_next = gsl_vector_alloc(n);
	gsl_vector* x_next = gsl_vector_alloc(n);
	gsl_vector* dx = gsl_vector_alloc(n);
	gsl_matrix* H = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	int N_steps = 0;
	int calls = 0;
	do {
//Newton step
		N_steps++;
		f(x,df,H); calls++;
		qr_gs_decomp(H,R);
		qr_gs_solve(H,R,df,dx);
//finding a good step by backtracking
		int backtracks = 0;
		do {
			gsl_vector_memcpy(x_next,x);
			gsl_vector_sub(x_next,dx);
			f(x_next,df_next,H); calls++;
			if (gsl_blas_dnrm2(df_next)<(1-1.0/pow(2,backtracks+1))*gsl_blas_dnrm2(df) || backtracks>6) break;
			backtracks++;
			gsl_vector_scale(dx,0.5);
		} while(1);
		fprintf(path_stream,"%g %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
		gsl_vector_memcpy(x,x_next);
		gsl_vector_memcpy(df,df_next);
	} while( gsl_blas_dnrm2(df)>epsilon );
//#Free the Memory
	gsl_vector_free(df);
	gsl_vector_free(df_next);
	gsl_vector_free(x_next);
	gsl_vector_free(dx);
	gsl_matrix_free(H);
	gsl_matrix_free(R);
	return N_steps;
}

double rosenbrock_H (const gsl_vector* x, gsl_vector* df, gsl_matrix* H) {
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double A = 100;
	double f0 = -2*(1-x0) - 4*A*(x1*x0-x0*x0*x0);
	double f1 = 2*A*(x1-x0*x0);
	gsl_vector_set(df, 0, f0);
	gsl_vector_set(df, 1, f1);
	gsl_matrix_set(H,0,0, 2 - 4*A*(x1-3*x0*x0) );
	gsl_matrix_set(H,1,0, -4*A*x0);
	gsl_matrix_set(H,0,1, -4*A*x0);
	gsl_matrix_set(H,1,1, 200);
	return (1-x0)*(1-x0)+A*(x1-x0*x0)*(x1-x0*x0);
}

double himmelblau_H (const gsl_vector* x, gsl_vector* df, gsl_matrix* H) {
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = 4*x0*(x0*x0+x1-11) + 2*(x0+x1*x1-7); 
	double f1 = 2*(x0*x0+x1-11) + 4*x1*(x0+x1*x1-7);
	gsl_vector_set(df, 0, f0);
	gsl_vector_set(df, 1, f1);
	gsl_matrix_set(H,0,0, 4*(3*x0*x0+x1-11) + 2 );
	gsl_matrix_set(H,1,0, 4*x0 + 4*x1);
	gsl_matrix_set(H,0,1, 4*x0 + 4*x1);
	gsl_matrix_set(H,1,1, 4*(x0+3*x1*x1-7) + 2 );
	return pow((x0*x0+x1-11),2) + pow((x0+x1*x1-7),2);
}

void finite_diff(double f(const gsl_vector* x), gsl_vector* x, gsl_vector* df) {
	double eps = sqrt(DBL_EPSILON);
	double f_i = f(x);
	for (int i=0; i<x->size; i++) {
		double xi = gsl_vector_get(x,i);
		double dx =fabs(xi)*eps;
		if (fabs(xi)<sqrt(eps)) dx = eps;
		gsl_vector_set(x,i,xi+dx);
		gsl_vector_set(df, i, (f(x)-f_i)/dx);
		gsl_vector_set(x,i,xi);
//		fprintf(stderr,"gradient: %g %g\n",gsl_vector_get(df,0),gsl_vector_get(df,1));
	}
	}

int quasi_newton (double f(const gsl_vector* x),
			gsl_vector* x, 
			double epsilon,
			FILE* path_stream) {
	int n = x->size;
	double eps = sqrt(DBL_EPSILON);
	gsl_vector* df = 	gsl_vector_alloc(n);
	gsl_vector* df_next = 	gsl_vector_alloc(n);
	gsl_vector* ddf =	gsl_vector_alloc(n);
	gsl_vector* x_next = 	gsl_vector_alloc(n);
	gsl_vector* dx = 	gsl_vector_alloc(n);
	gsl_vector* u = 	gsl_vector_alloc(n);
	gsl_matrix* B = 	gsl_matrix_alloc(n,n);
	gsl_matrix_set_identity(B);
	int N_steps = 0, calls = 0;
	finite_diff(f,x,df); calls+=2;
	double fx=f(x);
	double f_next;
	do {
//itterative update of Hessian
//Newton step
		N_steps++;
		gsl_blas_dgemv(CblasNoTrans,-1,B,df,0,dx); //dx = -B*df newton step
		if (gsl_blas_dnrm2(dx)<eps*gsl_blas_dnrm2(x)) {
			fprintf(stderr,"Minimization converged due to step length below sqrt(DBL_EPSILON): %g\n",gsl_blas_dnrm2(dx));
			break;}
		if (gsl_blas_dnrm2(df)<epsilon) {
			fprintf(stderr,"Minimization converged due to gradient below user defined precision: %g\n",gsl_blas_dnrm2(df));
			break;}
//finding a good step by backtracking
		double lambda = 1;
		do {
			gsl_vector_memcpy(x_next,x);
			gsl_vector_add(x_next,dx);
			f_next = f(x_next); calls++;
			double dxTg;
			gsl_blas_ddot(dx, df, &dxTg);
			if (f_next<fx+0.1*dxTg) break;
			if (lambda<eps) { fprintf(stderr,"bad backtracking when N=%i\n",N_steps); break;} //bad break...
			gsl_vector_scale(dx,0.5);
			lambda *= 0.5;
		} while(1);
//Updating Hessian with Broyden's update dH = c*dxT
		finite_diff(f,x_next,df_next); calls+=2;
		gsl_vector_memcpy(ddf, df_next); //ddf: change in gradient
		gsl_blas_daxpy(-1,df,ddf); //ddf = df_next-df
		gsl_vector_memcpy(u, dx); //u: good backtracking step
		gsl_blas_dgemv(CblasNoTrans,-1,B,ddf,1,u); // u=dx-B*ddf
		double dxTddf, uTddf;
		gsl_blas_ddot(dx,ddf,&dxTddf);
		if (fabs(dxTddf)>1e-12) {
			gsl_blas_ddot(u,ddf,&uTddf);
			double k=uTddf/(2*dxTddf);
			gsl_blas_daxpy(-k,dx,u); //u = (dx-B*ddf) - dx*(uTddf/(2*dxTddf))
			gsl_blas_dger(1.0/dxTddf,u,dx,B); //symmetric rank-1 update
			gsl_blas_dger(1.0/dxTddf,dx,u,B); //H = 1/dxTddf*dx*u^T+B
		}
		for (int i=0; i<n; i++) {
			fprintf(path_stream,"%g ",gsl_vector_get(x,i));
		} fprintf(path_stream,"\n");
		gsl_vector_memcpy(x,x_next);
		gsl_vector_memcpy(df,df_next);
	} while(N_steps<200);
//#Free the Memory
	gsl_vector_free(df);
	gsl_vector_free(df_next);
	gsl_vector_free(ddf);
	gsl_vector_free(x_next);
	gsl_vector_free(dx);
	gsl_vector_free(u);
	gsl_matrix_free(B);
	return N_steps;
}

double rosenbrock (const gsl_vector* x) {
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double A = 100;
	return (1-x0)*(1-x0)+A*(x1-x0*x0)*(x1-x0*x0);
}

double himmelblau (const gsl_vector* x) {
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	return pow((x0*x0+x1-11),2) + pow((x0+x1*x1-7),2);
}


