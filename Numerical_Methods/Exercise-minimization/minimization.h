#ifndef HAVE_MINIMIZATION_H
#define HAVE_MINIMIZATION_H

int newton (double f(const gsl_vector* x, gsl_vector* df, gsl_matrix* H), 
				gsl_vector* x, 
				double epsilon, 
				FILE* path_stream);

double rosenbrock_H 	(const gsl_vector* x, gsl_vector* df, gsl_matrix* H);
double himmelblau_H 	(const gsl_vector* x, gsl_vector* df, gsl_matrix* H);
double rosenbrock	(const gsl_vector* x);
double himmelblau	(const gsl_vector* x);

void finite_diff(double f(const gsl_vector* x), gsl_vector* x, gsl_vector* df);

int quasi_newton (double f(const gsl_vector* x), 
				gsl_vector* x, 
				double epsilon, 
				FILE* path_stream);
/*
int newton (void f(const gsl_vector* x, gsl_vector* fx),
				gsl_vector* x,
				double dx,
				double epsilon,
				FILE* path_stream);


*/
#endif

