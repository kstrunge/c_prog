#ifndef HAVE_NEWTON_H
#define HAVE_NEWTON_H

int newton_with_jacobian (void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J), 
				gsl_vector* x, 
				double epsilon, 
				FILE* path_stream);

void test_J 		(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
void rosenbrock_J 	(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
void himmelblau_J 	(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);


int newton_root (void f(const gsl_vector* x, gsl_vector* fx),
				gsl_vector* x,
				double dx,
				double epsilon,
				FILE* path_stream);

void test 	(const gsl_vector* x, gsl_vector* fx);
void rosenbrock_root	(const gsl_vector* x, gsl_vector* fx);
void himmelblau_root	(const gsl_vector* x, gsl_vector* fx);

#endif

