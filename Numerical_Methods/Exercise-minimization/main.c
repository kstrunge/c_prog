#include<stdio.h>
#include<stdlib.h>
#include<float.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include"lin_eq.h"
#include"minimization.h"
#include<gsl/gsl_blas.h>
#include"newton.h"

double him(double x, double y) {return (x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7);} 
double ros(double x, double y) {return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);}

double lsf_func(gsl_vector* x, double t) {
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	return A*exp(-t/T)+B;
	}

double lsf(const gsl_vector* x) {
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	
	double sum = 0;
	for (int i=0; i<N; i++) {
		sum += (y[i]-(A*exp(-t[i]/T)+B))*(y[i]-(A*exp(-t[i]/T)+B))/(e[i]*e[i]);
	}
	return sum;
	
}
 
int main(int argc, char** argv) {
// read and setup initial guess from "argv"
	int n = 2;
	gsl_vector* ros_x = gsl_vector_alloc(n);
	gsl_vector* him_x = gsl_vector_alloc(n);
	for (int i=0; i<n;i++) {
		gsl_vector_set(ros_x, i, atoi(argv[i+1]));
		gsl_vector_set(him_x, i, atoi(argv[i+3]));
	}
	int steps;
	double eps = sqrt(DBL_EPSILON);
	
	printf("Initial guesses:\n");
	printf("Rosenbrock:\n");
	print_vector(ros_x); printf("\n");	
	printf("Himmelblau:\n");
	print_vector(him_x); printf("\n");

//finding root of derivatives of rosenbrock
	FILE* stdros = fopen("rosenbrock.path","w");
	steps = newton (rosenbrock_H, ros_x, eps, stdros);
	double f_min = ros(gsl_vector_get(ros_x,0), gsl_vector_get(ros_x,1));
	fclose(stdros);
	printf("Resulting Minimum of the Rosenbrock Equation:\nFrom Wikipedia: Minimum at f(a,a^2)=0 and a=1\n");
	print_vector(ros_x);
	printf("Number of Minimization Steps: %i\n",steps);
	printf("The function value is %g\n",f_min);
	if (f_min>eps) printf(" - Warning: You might have found maximum instead");
	printf("\n\n");

//finding root of derivatives of himmelblau
	FILE* stdhim = fopen("himmelblau.path","w");
	steps = newton (himmelblau_H, him_x, eps, stdhim);
	f_min = him(gsl_vector_get(him_x,0), gsl_vector_get(him_x,1));
	fclose(stdhim);
	printf("Resulting Minimum of the Himmelblau Equation:\nFrom Wikipedia: Minima at f(3,2)=0, f(-2.805,3.131)=0, f(-3.779,-3.283)=0 and f(3.584,-1.848)=0.\n");
	print_vector(him_x);
	printf("Number of Minimization steps: %i\n",steps);
	printf("The function value is %g",f_min);
	if (f_min>eps) printf(" - Warning: You might have found maximum instead");
	printf("\n\n");

//Quasi newton methods
	for (int i=0; i<n;i++) {
		gsl_vector_set(ros_x, i, atoi(argv[i+1]));
		gsl_vector_set(him_x, i, atoi(argv[i+3]));
	}
//finding root of derivatives of rosenbrock
	FILE* stdros2 = fopen("rosenbrock2.path","w");
	steps = quasi_newton (rosenbrock, ros_x, eps, stdros2);
	f_min = ros(gsl_vector_get(ros_x,0), gsl_vector_get(ros_x,1));
	fclose(stdros2);
	printf("Resulting Minimum of the Rosenbrock Equation with broydens update on inverse Hessian:\nFrom Wikipedia: Minimum at f(a,a^2)=0 and a=1\n");
	print_vector(ros_x);
	printf("Number of Minimization Steps: %i\n",steps);
	printf("The function value is %g\n",f_min);
	if (f_min>eps) printf(" - Warning: You might have found maximum instead");
	printf("\n\n");

//finding root of derivatives of himmelblau
	FILE* stdhim2 = fopen("himmelblau2.path","w");
	steps = quasi_newton (himmelblau, him_x, eps, stdhim2);
	f_min = him(gsl_vector_get(him_x,0), gsl_vector_get(him_x,1));
	fclose(stdhim2);
	printf("Resulting Minimum of the Himmelblau Equation with Broydens update on inverse Hessian:\nFrom Wikipedia: Minimum at f(a,a^2)=0 and a=1\n");
	print_vector(him_x);
	printf("Number of Minimization Steps: %i\n",steps);
	printf("The function value is %g\n",f_min);
	if (f_min>eps) printf(" - Warning: You might have found maximum instead");
	printf("\n\n");



//rosenbrock contour plot
	double xbound = 10;
	double ybound = 80;
	FILE* ros_contour = fopen("contour_r.dat","w");
	for (double xx=-xbound; xx<xbound; xx+=xbound/100) {
		for (double yy=-ybound; yy<ybound; yy+=ybound/100) {
			double fxy = ros(xx,yy);
			fprintf(ros_contour,"%g %g %g\n",xx,yy,fxy);
		}
	}
	fclose(ros_contour);
//himmelblau contour plot
	double bound = 4;
	FILE* him_contour = fopen("contour_h.dat","w");
	for (double xx=-bound; xx<bound; xx+=bound/100) {
		for (double yy=-bound; yy<bound; yy+=bound/100) {
			double fxy = him(xx,yy);
			fprintf(him_contour,"%g %g %g\n",xx,yy,fxy);
		}
	}
	fclose(him_contour);

//Comparison with rootfinding
	for (int i=0; i<n;i++) {
		gsl_vector_set(ros_x, i, atoi(argv[i+1]));
		gsl_vector_set(him_x, i, atoi(argv[i+3]));}
	FILE* root = fopen("root.dat","w");
	steps = newton_with_jacobian(rosenbrock_J, ros_x, eps, root);
	printf("Rootfinding of rosenbrock with Jacobian finished in %i steps\n",steps);
	steps = newton_with_jacobian(himmelblau_J, him_x, eps, root);
	printf("Rootfinding of himmelblau with Jacobian finished in %i steps\n",steps);
	for (int i=0; i<n;i++) {
		gsl_vector_set(ros_x, i, atoi(argv[i+1]));
		gsl_vector_set(him_x, i, atoi(argv[i+3]));}
	steps = newton_root(rosenbrock_root, ros_x, eps, eps, root);
	printf("Rootfinding of rosenbrock with Finite Difference finished in %i steps\n",steps);
	steps = newton_root(himmelblau_root, him_x, eps, eps, root);
	printf("Rootfinding of himmelblau with Finite Difference finished in %i steps\n",steps);

// Free the Memory!
	gsl_vector_free(ros_x);
	gsl_vector_free(him_x);
//least square fitting
	int N = 3;
	gsl_vector* lsq = gsl_vector_alloc(N);
	printf("Least Square Method - Initial guess:\n");
	gsl_vector_set(lsq,0,1);
	gsl_vector_set(lsq,1,1);
	gsl_vector_set(lsq,2,1);
	print_vector(lsq);

	FILE* lsf_path = fopen("lsf.path","w");
	steps = quasi_newton(lsf,lsq,eps,lsf_path);
	printf("The least square minimization gives following parameters:\n");
	print_vector(lsq);
	printf("Minimal error: %g\n",lsf(lsq));
	fclose(lsf_path);
	
	FILE* model = fopen("lsf_model.dat","w");
	for (double x_lsf=0.23; x_lsf<9.77; x_lsf+=0.1) {
		fprintf(model,"%g %g\n",x_lsf,lsf_func(lsq,x_lsf));
	}
	fclose(model);
	
	gsl_vector_free(lsq);
return 0;
}

