#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>
#include"lin_eq.h"

int newton_with_jacobian (void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J), 
				gsl_vector* x, 
				double epsilon,
				FILE* path_stream) {
	int n = x->size;
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* fx_next = gsl_vector_alloc(n);
	gsl_vector* x_next = gsl_vector_alloc(n);
	gsl_vector* dx = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	int N_steps = 0;
	int calls = 0;
	do {
//Newton step
		N_steps++;
		f(x,fx,J); calls++;
		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,fx,dx);
		gsl_vector_scale(dx,-1);
//finding a good step by backtracking
		int backtracks = 0;
		do {
			gsl_vector_memcpy(x_next,x);
			gsl_vector_add(x_next,dx);
			f(x_next,fx_next,J); calls++;
			if (gsl_blas_dnrm2(fx_next)<(1-1.0/pow(2,backtracks+1))*gsl_blas_dnrm2(fx) || backtracks>6) break;
			backtracks++;
			gsl_vector_scale(dx,0.5);
		} while(1);
		fprintf(path_stream,"%g %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
		gsl_vector_memcpy(x,x_next);
		gsl_vector_memcpy(fx,fx_next);
	} while( gsl_blas_dnrm2(fx)>epsilon );
//#Free the Memory
	gsl_vector_free(fx);
	gsl_vector_free(fx_next);
	gsl_vector_free(x_next);
	gsl_vector_free(dx);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
//	int res[2] = {N_steps,calls}; 
	return N_steps;
}

void test_J (const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) {
	double A = 10000;
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1); 
	gsl_vector_set(fx,0,A*x0*x1-1);
	gsl_vector_set(fx,1,exp(-x0) + exp(-x1) - 1 - 1.0/A);
	for (int i=0; i<2; i++) {
	gsl_matrix_set(J,0,i,A*gsl_vector_get(x,1-i));
	gsl_matrix_set(J,1,i,-exp(gsl_vector_get(x,i)));
	}
}

void rosenbrock_J (const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) {
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double A = 100;
	double f0 = -2*(1-x0) - 4*A*(x1*x0-x0*x0*x0);
	double f1 = 2*A*(x1-x0*x0);
	gsl_vector_set(fx, 0, f0);
	gsl_vector_set(fx, 1, f1);
	gsl_matrix_set(J,0,0, 2 - 4*A*(x1-3*x0*x0) );
	gsl_matrix_set(J,1,0, -4*A*x0);
	gsl_matrix_set(J,0,1, -4*A*x0);
	gsl_matrix_set(J,1,1, 200);
}

void himmelblau_J (const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) {
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = 4*x0*(x0*x0+x1-11) + 2*(x0+x1*x1-7); 
	double f1 = 2*(x0*x0+x1-11) + 4*x1*(x0+x1*x1-7);
	gsl_vector_set(fx, 0, f0);
	gsl_vector_set(fx, 1, f1);
	gsl_matrix_set(J,0,0, 4*(3*x0*x0+x1-11) + 2 );
	gsl_matrix_set(J,1,0, 4*x0 + 4*x1);
	gsl_matrix_set(J,0,1, 4*x0 + 4*x1);
	gsl_matrix_set(J,1,1, 4*(x0+3*x1*x1-7) + 2 );
}


int newton_root (void f(const gsl_vector* x, gsl_vector* fx),
				gsl_vector* x,
				double dx,
				double epsilon,
				FILE* path_stream) {
	int n = x->size;
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* fx_next = gsl_vector_alloc(n);
	gsl_vector* x_next = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	int N_steps = 0;
	int calls = 0;
	do {
		N_steps++;
		f(x,fx); calls++;
//build J via finite difference method
		for (int i=0; i<n; i++) {
			gsl_vector_set(x,i, gsl_vector_get(x,i)+dx );
			f(x,df); calls++;
			gsl_vector_sub(df,fx);
			for(int j=0;j<n;j++) gsl_matrix_set(J,j,i, gsl_vector_get(df,j)/dx );
			gsl_vector_set(x,i, gsl_vector_get(x,i)-dx );
		}
//Newton step
		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1);
//finding a good step by backtracking
		int backtracks = 0;
		do {
			gsl_vector_memcpy(x_next,x);
			gsl_vector_add(x_next,Dx);
			f(x_next,fx_next); calls++;
			if (gsl_blas_dnrm2(fx_next)<(1-1.0/pow(2,backtracks+1))*gsl_blas_dnrm2(fx) || backtracks>6) break;
			backtracks++;
			gsl_vector_scale(Dx,0.5);
		} while(1);
		fprintf(path_stream,"%g %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
		gsl_vector_memcpy(x,x_next);
		gsl_vector_memcpy(fx,fx_next);
	} while( gsl_blas_dnrm2(fx)>epsilon );
//#Free the Memory
	gsl_vector_free(fx);
	gsl_vector_free(fx_next);
	gsl_vector_free(x_next);
	gsl_vector_free(Dx);
	gsl_vector_free(df);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
//	int res[2] = {N_steps,calls}; 
	return N_steps;
}


void test (const gsl_vector* x, gsl_vector* fx) {
	double A = 10000;
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1); 
	gsl_vector_set(fx,0,A*x0*x1-1);
	gsl_vector_set(fx,1,exp(-x0) + exp(-x1) - 1 - 1.0/A);
}

void rosenbrock_root (const gsl_vector* x, gsl_vector* fx) {
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double A = 100;
	double f0 = -2*(1-x0) - 4*A*(x1*x0-x0*x0*x0);
	double f1 = 2*A*(x1-x0*x0);
	gsl_vector_set(fx, 0, f0);
	gsl_vector_set(fx, 1, f1);
}

void himmelblau_root (const gsl_vector* x, gsl_vector* fx) {
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double f0 = 4*x0*(x0*x0 + x1-11) + 2*(x0 + x1*x1-7); 
	double f1 = 2*(x0*x0 + x1-11) + 4*x1*(x0 + x1*x1-7);
	gsl_vector_set(fx, 0, f0);
	gsl_vector_set(fx, 1, f1);
}
