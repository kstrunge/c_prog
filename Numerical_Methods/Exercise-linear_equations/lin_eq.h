#ifndef HAVE_LINEQ_H
#define HAVE_LINEQ_H

void print_matrix	(gsl_matrix* A);
void print_vector	(gsl_vector* v);
void qr_gs_decomp	(gsl_matrix* A, gsl_matrix* R);
void qr_backsups	(gsl_matrix* A, gsl_vector* R);
void qr_gs_solve	(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* x);
void qr_gs_inverse	(gsl_matrix* Q, gsl_matrix* R, gsl_matrix *B);


#endif
