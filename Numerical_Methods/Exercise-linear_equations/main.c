#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>
#include"lin_eq.h"

int not_equal (double a, double b) {
	double e = pow(10,-10);
	double diff = fabs(a-b);
	if (diff<e) return 0;
	else return 1;
	}

int main() {
//make random tall(n>m) matrix A
	int n = 5;
	int m = 3;
	gsl_matrix* A = gsl_matrix_alloc(n,m);
	for (int i=0;i<n;i++) {
		for (int j=0; j<m;j++) {
			gsl_matrix_set(A,i,j,(double)rand()/RAND_MAX);
		}
	}
	printf("Matrix A:\n");
	print_matrix(A); printf("\n");
	gsl_matrix* A_copy = gsl_matrix_alloc(n,m); 
	gsl_matrix_memcpy(A_copy, A);
//make QR decomposition of matrix A in Q(saved in A) and R
	gsl_matrix* R = gsl_matrix_alloc(m,m);
	qr_gs_decomp(A,R);
	printf("Matrix Q:\n");
	print_matrix(A); printf("\n");
	printf("Matrix R:\n");
	print_matrix(R); 

/*Check if R is upper triangular (R_{ij}=0, i<j) check if Q is orthogonal (Q^TQ=1) 
 and check if QR=A */
	int flag = 0;
        for(int i=0; i<m;i++) {
                for (int j=0; j<i; j++) {
//				printf("i = %i, j = %i\n",i,j);
				if(gsl_matrix_get(R,i,j)!=0.0) flag=1;
                        }
        }
	if (flag) printf("Test Failed - R is not upper triangular\n");
	else printf("Test Passed - R is upper triangular\n");
	printf("\n");

	gsl_matrix* QR = gsl_matrix_calloc(n,m);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,QR);
	printf("Matrix QR:\n");
	print_matrix(QR);
	int flag_QR = 0;
        for(int i=0; i<n;i++) {
                for (int j=0; j<m; j++) {
				if(not_equal(gsl_matrix_get(QR,i,j),gsl_matrix_get(A_copy,i,j))) flag_QR = 1;
                        }
        }
	if (flag_QR) printf("Test Failed - QR =/= A\n");
	else printf("Test Passed - QR = A\n");
	printf("\n");

	gsl_matrix* QQ = gsl_matrix_calloc(m,m);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,QQ);
	printf("Matrix Q^TQ:\n");
	print_matrix(QQ);
	int flag_QQ = 0;
        for(int i=0; i<m; i++) {
                for (int j=0; j<m; j++) {
				if((i==j) && (not_equal(gsl_matrix_get(QQ,i,j),1.0))) flag_QQ=1;
				if((i!=j) && (not_equal(gsl_matrix_get(QQ,i,j),0.0))) flag_QQ=1;
                        }
        }
	if (flag_QQ) printf("Test Failed - Q is not orhogonal\n");
	else printf("Test Passed - Q is orthogonal\n");
	printf("\n");
	gsl_matrix_free(A);
	gsl_matrix_free(A_copy);
	gsl_matrix_free(R);
	gsl_matrix_free(QR);
	gsl_matrix_free(QQ);

//2. make square matrix A and vector b
	gsl_matrix* Q = gsl_matrix_alloc(n,n);
	gsl_vector* b = gsl_vector_alloc(n);
	for (int i=0;i<n;i++) {
		gsl_vector_set(b,i,(double)rand()/RAND_MAX);
		for (int j=0; j<n;j++) {
			gsl_matrix_set(Q,i,j,(double)rand()/RAND_MAX);
		}
	}
	gsl_matrix* Q_copy = gsl_matrix_alloc(n,n); 
	gsl_matrix_memcpy(Q_copy, Q);
	printf("New (Square) Matrix A :\n");
	print_matrix(Q); printf("\n");
	printf("Vector b:\n");
	print_vector(b); printf("\n");

//use QR decomp to solve QRx=b
	gsl_matrix* R1 = gsl_matrix_alloc(n,n);
	gsl_vector* x = gsl_vector_alloc(n);
	qr_gs_decomp(Q,R1);
	qr_gs_solve(Q,R1,b,x);	
	printf("Solution Vector x:\n");
	print_vector(x); printf("\n");

//check if Ax=b
	gsl_vector* Ax = gsl_vector_alloc(n);
	gsl_blas_dgemv(CblasNoTrans,1.0,Q_copy,x,0.0,Ax);
	printf("Vector Ax (should be b):\n");
	print_vector(Ax); 
	int flag_x = 0;
	for (int i=0; i<n; i++) {
		if ( not_equal(gsl_vector_get(Ax,i), gsl_vector_get(b,i)) ) flag_x = 1;
	}
	if (flag_x) printf("Test Failed - Ax =/= b\n");
	else printf("Test Passed - Ax = b\n");
	printf("\n");

	gsl_matrix_free(Q);
	gsl_matrix_free(R1);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_matrix_free(Q_copy);
	gsl_vector_free(Ax);

//B: generate square A and decomp into QR
	gsl_matrix* A2 = gsl_matrix_alloc(n,n);
	for (int i=0;i<n;i++) {
		for (int j=0; j<n;j++) {
			gsl_matrix_set(A2,i,j,(double)rand()/RAND_MAX);
		}
	}
	printf("Matrix A for Inverting:\n");
	print_matrix(A2); printf("\n");

	gsl_matrix* A2_copy = gsl_matrix_alloc(n,n);
	gsl_matrix_memcpy(A2_copy, A2);
		
	gsl_matrix* R2 = gsl_matrix_alloc(n,n);
	gsl_matrix* B2 = gsl_matrix_alloc(n,n);
	qr_gs_decomp(A2,R2);
	
//find inverse matrix B by solving A*b_i=e_i
	qr_gs_inverse(A2,R2,B2);
	printf("Inverted Matrix B:\n");
	print_matrix(B2); printf("\n");
//check AB=I
	gsl_matrix* AB = gsl_matrix_alloc(n,n);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A2_copy,B2,0.0,AB);

	printf("Matrix AB (should be I):\n");
	print_matrix(AB); 
	int flag_AB = 0;
        for(int i=0; i<m; i++) {
                for (int j=0; j<m; j++) {
				if((i==j) && (not_equal(gsl_matrix_get(AB,i,j),1.0))) flag_AB=1;
				if((i!=j) && (not_equal(gsl_matrix_get(AB,i,j),0.0))) flag_AB=1;
                        }
        }
	if (flag_AB) printf("Test Failed - AB=/=I\n");
	else printf("Test Passed - AB=I\n");
	printf("\n");
	
	
//Free the memory
	gsl_matrix_free(A2);
	gsl_matrix_free(A2_copy);
	gsl_matrix_free(R2);
	gsl_matrix_free(B2);
	gsl_matrix_free(AB);


return 0;
}
