#include<math.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include"ode.h"

void trigonometric(double t, gsl_vector* y, gsl_vector* dydt) {
	double y0 = gsl_vector_get(y,0);
	double y1 = gsl_vector_get(y,1);
	gsl_vector_set(dydt,0,y1);
	gsl_vector_set(dydt,1,-y0);
}

void pendulum(double t, gsl_vector* y, gsl_vector* dydt) {
	double y0 = gsl_vector_get(y,0);
	double y1 = gsl_vector_get(y,1);
	double g = 9.807;
	double l = 1;
	double m = 1;
	double b = 0.2;
	gsl_vector_set(dydt,0,y1);
	gsl_vector_set(dydt,1,-g/l*sin(y0)-b/m*y1);
}

int main() {
	int n = 2;
	int path_size = 10000; //increase this if stack smashing
	double path[path_size*n];
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector_set(y,0,-3);
	gsl_vector_set(y,1,5);
	double a = 0, b = 15, s = .1, acc = 3e-2, eps = 3e-2;
	
	driver(&a, b, &s, y, acc, eps, rkstep38, pendulum, path, &path_size);
	
	for (int i=0; i<path_size; i++) {
		if (path[2*i]!=0) {
			for (int j=0; j<n; j++) fprintf(stderr,"%g ",path[2*i+j]); 
			fprintf(stderr,"\n");
		}
	}
	

	FILE* lotka_stream = fopen("lotka_volterra.data","w");
	void lotka_volterra(double t, gsl_vector* y, gsl_vector* dydt) {
		double y0 = gsl_vector_get(y,0);
		double y1 = gsl_vector_get(y,1);
		double a = 2;
		double b = 0.2;
		double c = 0.1;
		double d = 2.5;
		gsl_vector_set(dydt,0,a*y0-b*y0*y1);
		gsl_vector_set(dydt,1,c*y0*y1-d*y1);
	
		fprintf(lotka_stream, "%g %g %g\n", t, y0, y1);
	}
	n = 2;
	int path_size_2 = 1000;
	double path_2[path_size_2*n];
	gsl_vector_set(y,0,1);
	gsl_vector_set(y,1,1);
	a=0, s=0.1, acc=5e-1, eps=5e-1;
	driver(&a, b, &s, y, acc, eps, rkstep38, lotka_volterra, path_2, &path_size_2);
	
return 0;
}
