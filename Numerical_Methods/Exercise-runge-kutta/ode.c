#include"ode.h"
#include<math.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>

void rkstep12(double t,						
		double h, 						
		gsl_vector* yt,						
		void f(double t, gsl_vector* y, gsl_vector* dydt),	
		gsl_vector* yth,						
		gsl_vector* err						
		) {
	int n = yt->size;
	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* k1 = gsl_vector_alloc(n);

	f(t,yt,k0); //k0 = f(t,y)
	gsl_vector_memcpy(k1,yt);
	gsl_blas_daxpy(h/2, k0, k1); //k1 = (h/2)*k0+yt
	f(t+h/2, k1, k1); //k1 = f(t+h/2, (h/2)*k0+yt)
	gsl_vector_memcpy(yth,yt);
	gsl_blas_daxpy(h,k1,yth); //yth = h*k1+yt
	gsl_vector_sub(k0,k1); //k0 = k0-k1
	gsl_vector_scale(k0,h/2); //k0 = (k0-k1)*h/2
	gsl_vector_memcpy(err,k0);
	
	gsl_vector_free(k0);
	gsl_vector_free(k1);
}

void rkstep38(double t,						
		double h, 						
		gsl_vector* yt,						
		void f(double t, gsl_vector* y, gsl_vector* dydt),	
		gsl_vector* yth,						
		gsl_vector* err						
		) {
	int n = yt->size;
//Setting up Butcher Tableau (Runge-Kutta matrix, weights and nodes):
	int s = 5;
	gsl_matrix* A = gsl_matrix_alloc(s,s);
	gsl_vector* b = gsl_vector_calloc(s);
	gsl_vector* c = gsl_vector_calloc(s);
	gsl_matrix_set(A,2,1, 1.0/3.0);
	gsl_matrix_set(A,3,1, -1.0/3.0);
	gsl_matrix_set(A,3,2, 1.0);
	gsl_matrix_set(A,4,1, 1.0);
	gsl_matrix_set(A,4,2, -1.0);
	gsl_matrix_set(A,4,3, 1.0);
	gsl_vector_set(b,1, 1.0/8.0);
	gsl_vector_set(b,2, 3.0/8.0);
	gsl_vector_set(b,3, 3.0/8.0);
	gsl_vector_set(b,4, 1.0/8.0);
	gsl_vector_set(c,1, 0.0);
	gsl_vector_set(c,2, 1.0/3.0);
	gsl_vector_set(c,3, 2.0/3.0);
	gsl_vector_set(c,4, 1.0);
//Making the k_i vectors
	gsl_matrix* k = gsl_matrix_calloc(n,s);
	gsl_vector* v_temp = gsl_vector_alloc(n);
	for (int i=1;i<s;i++) {
		f(t+gsl_vector_get(c,i)*h, yt, v_temp);
		for (int ni=0;ni<n;ni++) {
			double sum=0;
			for (int j=1; j<i; j++) {
				sum += gsl_matrix_get(A,i,j)*gsl_matrix_get(k,ni,j);
//				printf("i,j,n: %i,%i,%i\n",i,j,ni);
			}
//			printf("sum: %g\n",sum);
			gsl_matrix_set(k,ni,i, gsl_vector_get(v_temp,ni)+h*sum);
		}
	}
//	fprint_matrix(stdout,k);
//Calculating next value yth = yt + h*sum(b_i*k_i)
	for (int ni=0;ni<n;ni++) {
		double sum=0;
		for (int i=1;i<s; i++) {
			sum += gsl_vector_get(b,i)*gsl_matrix_get(k,ni,i);
		}
		gsl_vector_set(yth, ni, gsl_vector_get(yt,ni) + h*sum);
	}
	
//Estimating error via two-stepper method (h_err = h/2)
	h = h/2;
	gsl_vector* yt_err = gsl_vector_alloc(n);
//From t to t+h/2
	for (int i=1;i<s;i++) {
		f(t+gsl_vector_get(c,i)*h, yt, v_temp);
		for (int ni=0;ni<n;ni++) {
			double sum=0;
			for (int j=1; j<i; j++) {
				sum += gsl_matrix_get(A,i,j)*gsl_matrix_get(k,ni,j);
			}
			gsl_matrix_set(k,ni,i, gsl_vector_get(v_temp,ni)+h*sum);
		}
	}
	for (int ni=0;ni<n;ni++) {
		double sum=0;
		for (int i=1;i<s; i++) {
			sum += gsl_vector_get(b,i)*gsl_matrix_get(k,ni,i);
		}
		gsl_vector_set(yt_err, ni, gsl_vector_get(yt,ni) + h*sum);
	}
//From t+h/2 to t+h
	t = t+h/2;
	for (int i=1;i<s;i++) {
		f(t+gsl_vector_get(c,i)*h, yt_err, v_temp);
		for (int ni=0;ni<n;ni++) {
			double sum=0;
			for (int j=1; j<i; j++) {
				sum += gsl_matrix_get(A,i,j)*gsl_matrix_get(k,ni,j);
			}
			gsl_matrix_set(k,ni,i, gsl_vector_get(v_temp,ni)+h*sum);
		}
	}
	for (int ni=0;ni<n;ni++) {
		double sum=0;
		for (int i=1;i<s; i++) {
			sum += gsl_vector_get(b,i)*gsl_matrix_get(k,ni,i);
		}
		gsl_vector_set(yt_err, ni, gsl_vector_get(yt_err,ni) + h*sum - gsl_vector_get(yt,ni));
	}

	gsl_vector_memcpy(err,yt_err);
/*
	f(t,yt,k0); //k0 = f(t,y)
	gsl_vector_memcpy(k1,yt);
	gsl_blas_daxpy(h/2, k0, k1); //k1 = (h/2)*k0+yt
	f(t+h/2, k1, k1); //k1 = f(t+h/2, (h/2)*k0+yt)
	gsl_vector_memcpy(yth,yt);
	gsl_blas_daxpy(h,k1,yth); //yth = h*k1+yt
	gsl_vector_sub(k0,k1); //k0 = k0-k1
	gsl_vector_scale(k0,h/2); //k0 = (k0-k1)*h/2
	gsl_vector_memcpy(err,k0);
*/	
	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_vector_free(c);
	gsl_matrix_free(k);
	gsl_vector_free(v_temp);
	gsl_vector_free(yt_err);
}

void driver(double* t,
	double b,
	double* h,
	gsl_vector* yt,
	double acc,
	double eps,
	void stepper(double t, 
		double h, 
		gsl_vector* yt, 
		void f(double t, gsl_vector* y, gsl_vector* dydt), 
		gsl_vector* yth, 
		gsl_vector* err),
		void f(double t, gsl_vector* yt, gsl_vector* dydt),
		double path[],
		int* p_size
		){
	int n = yt->size;
	gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	double a = *t, t0, step;//, tau;
	int N_steps = 0, path_counter = 0;
	while (N_steps<10000000) {
//		fprintf(stderr,"%i\n",N_steps);
		t0 = *t;
		step = *h;
		if (t0>=b) break;
		if (t0+step > b) step = b-t0;
		stepper(t0, step, yt, f, yth, err);
// check if step meets the required accuracy with a more advanced stepper
		double tau_error_min = INFINITY;
		int tau_flag = 1;
		for (int i=0;i<n;i++) {
			double tau_k = (acc+eps*fabs(gsl_vector_get(yth,i)))*sqrt(step/(b-a));
			if (fabs(gsl_vector_get(err,i)) >= tau_k ) {
				tau_flag = 0;
			}
			if (fabs(tau_k/gsl_vector_get(err,i)) < tau_error_min) {
				tau_error_min = fabs(tau_k/gsl_vector_get(err,i));
			} 
		}
//		double err_norm = gsl_blas_dnrm2(err);
//		tau = gsl_blas_dnrm2(yt)*eps*sqrt(step/(b-a));
//		if (tau < acc*sqrt(step/(b-a))) tau = acc*sqrt(step/(b-a));
		if (tau_flag) {
			if (N_steps<*p_size*1000 && N_steps%1000==0) {
				path_counter++;
				for (int i=0; i<n; i++) *(path+(n*path_counter+i)) = gsl_vector_get(yt,i);
			}
			N_steps++;
			*t = t0+step;
			gsl_vector_memcpy(yt,yth);
//			fprintf(stderr,"%g %g\n",*t, gsl_vector_get(yt,0));
//			fprintf(stderr,"%g %g\n",gsl_vector_get(yt,0), gsl_vector_get(yt,1));
		}
// Updating stepsize
		if (err==0) *h *=2;
		else *h *= pow(tau_error_min,0.25)*0.95;
//		else *h *= pow(tau/err_norm,0.25)*0.95;
	}
	*p_size = path_counter+1;
	gsl_vector_free(yth);
	gsl_vector_free(err);
}

