#include<math.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include"ode.h"

void trigonometric(double t, gsl_vector* y, gsl_vector* dydt) {
	double y0 = gsl_vector_get(y,0);
	double y1 = gsl_vector_get(y,1);
	gsl_vector_set(dydt,0,y1);
	gsl_vector_set(dydt,1,-y0);
}

void pendulum(double t, gsl_vector* y, gsl_vector* dydt) {
	double y0 = gsl_vector_get(y,0);
	double y1 = gsl_vector_get(y,1);
	double g = 9.807;
	double l = 1;
	double m = 1;
	double b = 0.2;
	gsl_vector_set(dydt,0,y1);
	gsl_vector_set(dydt,1,-g/l*sin(y0)-b/m*y1);
}

int main() {
	int n = 2;
	int path_size = 1000; //increase this if stack smashing
	double path[path_size*n];
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector_set(y,0,-3);
	gsl_vector_set(y,1,5);
	double a = 0, b = 15, s = .1, acc = 1e-2, eps = 1e-2;
	
	driver(&a, b, &s, y, acc, eps, rkstep12, pendulum, path, &path_size);
	
	for (int i=0; i<path_size; i++) {
		for (int j=0; j<n; j++) fprintf(stderr,"%g ",path[2*i+j]); 
		fprintf(stderr,"\n");
	}
	
return 0;
}
