#include"ode.h"
#include<math.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>

void rkstep12(double t,						
		double h, 						
		gsl_vector* yt,						
		void f(double t, gsl_vector* y, gsl_vector* dydt),	
		gsl_vector* yth,						
		gsl_vector* err						
		) {
	int n = yt->size;
	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* k1 = gsl_vector_alloc(n);

	f(t,yt,k0); //k0 = f(t,y)
	gsl_vector_memcpy(k1,yt);
	gsl_blas_daxpy(h/2, k0, k1); //k1 = (h/2)*k0+yt
	f(t+h/2, k1, k1); //k1 = f(t+h/2, (h/2)*k0+yt)
	gsl_vector_memcpy(yth,yt);
	gsl_blas_daxpy(h,k1,yth); //yth = h*k1+yt
	gsl_vector_sub(k0,k1); //k0 = k0-k1
	gsl_vector_scale(k0,h/2); //k0 = (k0-k1)*h/2
	gsl_vector_memcpy(err,k0);
	
	gsl_vector_free(k0);
	gsl_vector_free(k1);
}

void driver(double* t,
	double b,
	double* h,
	gsl_vector* yt,
	double acc,
	double eps,
	void stepper(double t, 
		double h, 
		gsl_vector* yt, 
		void f(double t, gsl_vector* y, gsl_vector* dydt), 
		gsl_vector* yth, 
		gsl_vector* err),
		void f(double t, gsl_vector* yt, gsl_vector* dydt),
		double path[],
		int* p_size
		){
	int n = yt->size;
	gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	double a = *t, tau, t0, step;
	int N_steps = 0;
	while (N_steps<1000) {
//		fprintf(stderr,"%i\n",N_steps);
		t0 = *t;
		step = *h;
		if (t0>=b) break;
		if (t0+step > b) step = b-t0;
		stepper(t0, step, yt, f, yth, err);
// check if step meets the required accuracy
		double err_norm = gsl_blas_dnrm2(err);
		tau = gsl_blas_dnrm2(yt)*eps*sqrt(step/(b-a));
		if (tau < acc*sqrt(step/(b-a))) tau = acc*sqrt(step/(b-a));
		if (err_norm<tau) {
			if (N_steps<*p_size) {
				for (int i=0; i<n; i++) *(path+(n*N_steps+i)) = gsl_vector_get(yt,i);
			}
			N_steps++;
			*t = t0+step;
			gsl_vector_memcpy(yt,yth);
//			fprintf(stderr,"%g %g\n",*t, gsl_vector_get(yt,0));
//			fprintf(stderr,"%g %g\n",gsl_vector_get(yt,0), gsl_vector_get(yt,1));
		}
// Updating stepsize
		if (err==0) *h *=2;
		else *h *= pow(tau/err_norm,0.25)*0.95;
	}
	*p_size = N_steps;
	gsl_vector_free(yth);
	gsl_vector_free(err);
}


void fprint_vector(FILE* stream, gsl_vector* v) {
	for (int i=0; i<v->size; i++) fprintf(stream," %g",gsl_vector_get(v,i));
	fprintf(stream,"\n");
}
