#ifndef HAVE_ODE_H
#define HAVE_ODE_H
#include<gsl/gsl_vector.h>
void rkstep12(double t,						/*value of variable*/
	double h, 						/*stepsize*/
	gsl_vector* yt,						/*current value of y(t)*/
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/*gradient dydt = f(t,y)*/
	gsl_vector* yth,					/*next value of y(t)*/
	gsl_vector* err						/*error estimate dy*/
	);

void driver(double* t,
	double b,
	double* h,
	gsl_vector* yt,
	double acc,
	double eps,
	void stepper(double t, 
		double h, 
		gsl_vector* yt, 
		void f(double t, gsl_vector* y, gsl_vector* dydt), 
		gsl_vector* yth, 
		gsl_vector* err),
	void f(double t, gsl_vector* yt, gsl_vector* dydt),
	double path[],
	int* p_size
	);

void fprint_vector(FILE* stream, gsl_vector* v);
#endif
