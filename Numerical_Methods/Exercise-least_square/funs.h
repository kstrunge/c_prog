#ifndef HAVE_FUNS_H
#define HAVE_FUNS_H
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

double funs (int i, double x);
double func_eval (gsl_vector* xx, double x);
double error_eval (gsl_matrix* S, double x);

#endif

