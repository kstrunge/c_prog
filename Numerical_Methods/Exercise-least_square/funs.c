#include<math.h>
#include"funs.h"
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

double funs(int i, double x) {
	switch(i){
	case 0: return log(x); break;
	case 1: return 1.0;   break;
	case 2: return x;     break;
	default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
	}
}

double func_eval(gsl_vector* xx, double x) {
	int m = xx->size;
	double sum = 0;
	for (int i=0; i<m; i++) {
		sum += gsl_vector_get(xx,i)*funs(i,x);
	}
	return sum;
}

double error_eval(gsl_matrix* S, double x) {
	int m=S->size1;
	double sum = 0;
	for (int i=0; i<m; i++) {
		for (int j=0; j<m; j++) {
			sum+=funs(i,x)*gsl_matrix_get(S,i,j)*funs(j,x);
		}
	}
	return sqrt(sum);
}
