#include"funs.h"
#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include"lin_eq.h"
#include<gsl/gsl_blas.h>

int main(int argc, char** argv) {
/* Read Input */
	const int m = 3;
        const int n = atoi(argv[1]);
	double xdata[n], ydata[n], dydata[n];
        for (int i=0; i<n; i++) {
                scanf("%lg %lg %lg\n",&xdata[i], &ydata[i], &dydata[i]);
        }
//setup matrices
	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_matrix* R = gsl_matrix_alloc(m,m);
	gsl_matrix* R_copy = gsl_matrix_alloc(m,m);
	gsl_vector* xx = gsl_vector_alloc(m);
	gsl_vector* b = gsl_vector_alloc(n);
	for (int i=0;i<n;i++) {
		gsl_vector_set(b,i,ydata[i]);
		for (int j=0;j<m;j++) {
			gsl_matrix_set(A,i,j,funs(j,xdata[i]));
		}
	}

//QR decomposition and solving Ax=y
	qr_gs_decomp(A,R);
	gsl_matrix_memcpy(R_copy,R);
	qr_gs_solve(A,R,b,xx);	
	printf("c_k coefficinets:\n");
	print_vector(xx); printf("\n");

//Make covariance matrix as R^T*R
	gsl_matrix* RTR = gsl_matrix_alloc(m,m);
	gsl_matrix* RRTR = gsl_matrix_alloc(m,m);
	gsl_matrix* S = gsl_matrix_alloc(m,m);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,R_copy,R,0.0,RTR);
	qr_gs_decomp(RTR,RRTR);
	qr_gs_inverse(RTR,RRTR,S);
	printf("Covariance Matrix S:\n");
	print_matrix(S); printf("\n");

//print output to plot
	for (double x=xdata[0]; x<xdata[n-1]; x+=0.1) {
		double f = func_eval(xx,x);
		double e = error_eval(S,x);
		fprintf(stderr,"%g %g %g %g\n",x,f,f+e,f-e);
	}

// Free the Memory!
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(R_copy);
	gsl_vector_free(xx);
	gsl_vector_free(b);
	gsl_matrix_free(RTR);
	gsl_matrix_free(RRTR);
return 0;
}

