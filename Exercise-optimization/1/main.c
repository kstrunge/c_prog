#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#include<math.h>
#define TYPE gsl_multimin_fminimizer_nmsimplex2

double my_f(const gsl_vector *v, void *params) {
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	double f = (1-x)*(1-x)+100*(y-x*x)*(y-x*x);
	return f;
}

int main(int argc, char** argv) {
	const int dim = 2;

	gsl_multimin_function F;
	F.f = my_f;
	F.n = dim;
	
	gsl_multimin_fminimizer *M;
	
	M = gsl_multimin_fminimizer_alloc(TYPE,dim);
	gsl_vector* start = gsl_vector_alloc(dim);
	gsl_vector* step = gsl_vector_alloc(dim);
	gsl_vector_set(start,0,2);
	gsl_vector_set(start,1,4);
	gsl_vector_set(step,0,0.1);
	gsl_vector_set(step,1,0.1);

	gsl_multimin_fminimizer_set(M,&F,start,step);

	int iter=0; int status;
	double size;
	do{
		iter++;
		status = gsl_multimin_fminimizer_iterate(M);
		if (status) break;
		size = gsl_multimin_fminimizer_size (M);
		status = gsl_multimin_test_size (size, 1e-2);

		if (status == GSL_SUCCESS)

        {
          fprintf (stderr,"converged to minimum at:\n");
        }

      fprintf (stdout,"%g %g %g\n",
              gsl_vector_get (M->x, 0),
              gsl_vector_get (M->x, 1),
              M->fval);
    }
  while (status == GSL_CONTINUE && iter < 100);
	
      fprintf (stderr,"iter=%5d x=%g y=%g master=%g size=%g\n",
		iter, 
		gsl_vector_get (M->x, 0),
              	gsl_vector_get (M->x, 1),
              	M->fval,
		size);

return 0;
}
