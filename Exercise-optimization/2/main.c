#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

struct expdata {int n; double *t,*y,*e;};

double fitter(const gsl_vector *x, void *params) {
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	
	struct expdata *data = (struct expdata *)params;
	double *t=data->t;
	double *y=data->y;
	double *e=data->e;
	double deviation=0;

	for (int i=0;i<data->n;i++){
		deviation += pow((A*exp(-t[i]/T) + B - y[i]) / e[i], 2);
	}
	return deviation;
}


int main(int argc, char** argv) {

	FILE* log_stream = fopen("log","w");
	if(argc<2){
		fprintf(log_stream,"usage: %s number_of_lines_to_read\n",argv[0]);
}
	const int n=atoi(argv[1]);
	double t[n],y[n],e[n];
	for(int i=0;i<n;i++)
		scanf("%lg %lg %lg",t+i,y+i,e+i);

	for(int i=0;i<n;i++)
		fprintf(stderr,"%lg %lg %lg\n",t[i],y[i],e[i]);

	const int dim = 3;
	struct expdata data;
	data.n = n;
	data.t = t;
	data.y = y;
	data.e = e;
	
	gsl_multimin_function F;
	F.f = fitter;
	F.n = dim;
	F.params = (void*)&data;
	
	gsl_multimin_fminimizer *M;
	#define TYPE gsl_multimin_fminimizer_nmsimplex2
	M = gsl_multimin_fminimizer_alloc(TYPE,dim);
	gsl_vector* start = gsl_vector_alloc(dim);
	gsl_vector* step = gsl_vector_alloc(dim);
	gsl_vector_set(start,0,10);
	gsl_vector_set(start,1,1);
	gsl_vector_set(start,2,10);
	gsl_vector_set(step,0,1);
	gsl_vector_set(step,1,0.1);
	gsl_vector_set(step,2,1);

	gsl_multimin_fminimizer_set(M,&F,start,step);
	
	int iter=0;
	int status;
	double size;

	do {
		iter++;
		status = gsl_multimin_fminimizer_iterate(M);
		if (status) break;

		size = gsl_multimin_fminimizer_size(M);
		status = gsl_multimin_test_size(size,1e-2);
		if (status == GSL_SUCCESS) fprintf(log_stream,"converged at minimum\n");
		fprintf (log_stream,"iter=%5d A=%g T=%g B=%g fit=%g size=%g\n",
              		iter,
              		gsl_vector_get (M->x, 0),
              		gsl_vector_get (M->x, 1),
              		gsl_vector_get (M->x, 2),
              		M->fval, size);
	}while(status == GSL_CONTINUE && iter<200);
	fclose(log_stream);	
	double A = gsl_vector_get(M->x,0);	
	double T = gsl_vector_get(M->x,1);	
	double B = gsl_vector_get(M->x,2);	

	for(double i=0; i<=t[n-1]; i+=0.1) {
	fprintf(stdout,"%g %g\n", i, A*exp(-i/T) + B);
	}
		
return 0;
}

