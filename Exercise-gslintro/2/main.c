#include<gsl/gsl_linalg.h>
#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

void my_matrix_print(const char* s, gsl_matrix *A){
	printf("%s\n",s);
	for(int i=0; i<A->size1; i++){
		for(int j=0; j<A->size2; j++){
			printf("%8.3g",gsl_matrix_get(A,i,j));
		}
	printf("\n");
	}
}
void my_vector_print(const char* s, gsl_vector *v){
	printf("%s\n",s);
	for(int i=0; i<v->size; i++){
		printf("%8.3g",gsl_vector_get(v,i));
		}
	printf("\n");
}

int main(){
//INITIALIZE AND ALLOC
	const int n=3;
	gsl_matrix *A = gsl_matrix_alloc(n, n);
	gsl_matrix *B = gsl_matrix_alloc(n, n);
	gsl_vector *x = gsl_vector_alloc(n);
	gsl_vector *b = gsl_vector_alloc(n);
	double i;

//READ A FROM "A_input.txt"
	gsl_matrix_fscanf(stdin,A);
	my_matrix_print("A=", A);

//READ b FROM "b_input.txt"
	gsl_vector_fscanf(stdin,b);
	my_vector_print("b=",b);

//SOLVE LINEAR EQUATIONS
	gsl_matrix_memcpy(B, A);
	gsl_linalg_HH_solve(B, b, x);

//PRINT SOLUTION x
	my_vector_print("x=",x);
	gsl_blas_dgemv(CblasNoTrans,1,A,x,0,b);
	my_vector_print("Ax=",b);

//FREE THE MEMORY!
	gsl_matrix_free(A);
	gsl_matrix_free(B);
	gsl_vector_free(x);
	gsl_vector_free(b);	
return 0;
}
