#include<limits.h>
#include<float.h>
#include<stdio.h>

int main(){
	int max = INT_MAX/200;
	float sum_up_float = 0;
	int i=1;
	while (i <= max) {
		sum_up_float+=1.0f/i;
		i += 1;
		}
	
	printf("sum_up_float: %.15g\n", sum_up_float);
	
	float sum_down_float = 0;
	i=max;
	while (i > 0) {
		sum_down_float += 1.0f/i;
		i -= 1;
	}

	printf("sum_down_float: %.15g\n", sum_down_float);
	
	double sum_up_double = 0;
	i=1;
	while (i <= max) {
		sum_up_double+=1.0f/i;
		i += 1;
		}
	
	printf("sum_up_double: %.15g\n", sum_up_double);
	
	double sum_down_double = 0;
	i=max;
	while (i > 0) {
		sum_down_double += 1.0f/i;
		i -= 1;
	}

	printf("sum_down_double: %.15g\n", sum_down_double);

	return 0;
}
