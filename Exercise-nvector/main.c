#include<stdio.h>
#include "nvector.h"
#include<math.h>
#include<stdlib.h>
#define RND (double)rand()/RAND_MAX

int main() {
	int n = 3;
	
	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if (vi==value) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_add ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}
	nvector_add(a, b);
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);
	
	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_sub ...\n");
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x - y);
	}
	nvector_sub(a, b);
	nvector_print("a-b should   = ", c);
	nvector_print("a-b actually = ", a);
	
	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");
	
	
	printf("\nmain: testing nvector_scale ...\n");
	double x = RND;
	for (int i = 0; i < n; i++) {
		double y = RND;
		nvector_set(a, i, y);
		nvector_set(c, i, x*y);
	}

	
	nvector_scale(a, x);
	nvector_print("x*a should   = ", c);
	nvector_print("x*a actually = ", a);
	
	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");
      	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);

	return 0;
}
