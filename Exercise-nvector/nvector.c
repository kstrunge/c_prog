#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include "nvector.h"

/*
void komplex_print (komplex z) {
printf("(%g + %g*i)\n",z.re,z.im);
}

komplex komplex_new(double x, double y) {
	komplex z;
	z.re = x;
	z.im =y;
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	z->re = x;
	z->im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re, a.im + b.im };
	return result;
}

komplex komplex_sub (komplex a, komplex b) {
	komplex result = { a.re - b.re, a.im - b.im };
	return result;
}*/

nvector* nvector_alloc (int n) {
	nvector *v = (nvector *) malloc(sizeof(nvector));
	
	v->size = n;
	v->data = (double *)malloc(sizeof(double)*n);
	return v;
}

void    nvector_free (nvector *v) {
	free(v->data);
	free(v);
}

void    nvector_set (nvector *v, int i, double value) {
	assert( 0 <= i && i < v->size );
	v->data[i]=value;
}

double nvector_get (nvector *v, int i) {	
	assert( 0 <= i && i < v->size );
	return v->data[i];
}

double nvector_dot_product (nvector *u, nvector *v) {
	assert( v->size==u->size  );
	double result = 0;
	for (int i=0; i<(v->size); i++) {
		result += nvector_get(u, i)*nvector_get(v, i);
	}
	return result;
}

//Optional
void nvector_print (char *s, nvector *v){
	printf("%s",s);
        for (int i=0; i<(v->size); i++) {
                printf("(%.2g)",nvector_get(v, i));
	}
	printf("\n");
}

void nvector_set_zero (nvector *v){
	for (int i=0; i<(v->size); i++) {
		v->data[i]=0;
	}
}

int nvector_equal (nvector *a, nvector *b){
	if (a->size != b->size) {
		return 0;
	}
	for (int i=0; i<(a->size); i++) {
		if (a->data[i] != b->data[i]){
			return 0;
		}
	}
	return 1;
}

void nvector_add (nvector *a, nvector *b){
	assert(a->size == a->size);
	for (int i=0; i<(a->size); i++) {
		a->data[i] = a->data[i]+b->data[i];
	}
}
void nvector_sub (nvector *a, nvector *b){
	assert(a->size == a->size);
	for (int i=0; i<(a->size); i++) {
		a->data[i] = a->data[i]-b->data[i];
	}
}
void nvector_scale (nvector *a, double x){
	for (int i=0; i<(a->size); i++) {
		a->data[i]=a->data[i]*x;
	}
}

