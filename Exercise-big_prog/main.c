#include<stdio.h>
#include<math.h>
#include<gsl/gsl_sf.h>
double func(double k,double phi);

int main () {
	for (double k=0.01; k<1; k+=0.01) {
		printf("%g %g %g\n", k, func(k, M_PI/2), gsl_sf_ellint_F(M_PI/2, k, GSL_PREC_DOUBLE));
	}	
	for (double phi=-M_PI; phi<M_PI; phi+=0.05) {
		fprintf(stderr, "%g %g %g\n", phi, func(0.5, phi), gsl_sf_ellint_F(phi, 0.5, GSL_PREC_DOUBLE));

	}
return 0;
}
