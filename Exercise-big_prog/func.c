#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<math.h>

double integrand (double t, void *params) {
	double k = *(double*)params;
	double f = 1/sqrt(1-k*k*sin(t)*sin(t));
	return f;
}

double func (double k, double phi) {
	int limit = 100;
	gsl_integration_workspace *w;
	w = gsl_integration_workspace_alloc(limit);
	
	gsl_function F;
	F.function = integrand;
	F.params = (void *)&k;
	
	double result,error,acc=1e-8,eps=1e-8;
	int flag = gsl_integration_qags(&F, 0, phi, acc, eps, limit, w, &result, &error);

	gsl_integration_workspace_free(w);
	
	if(flag!=GSL_SUCCESS) return NAN;
	return result;


}

