#include<stdlib.h>
#include<stdio.h>
#include<math.h>

struct dart{
	double n; 
	int circle;
	uint seed;
	};

void* circler(void* param) {
	struct dart* data = (struct dart*)param;
	double n = data->n;
	int inside = 0;

	const int mid = n/2;
	#pragma omp parallel sections reduction(+:inside)
	{
	#pragma omp section
	{
		for (int i = 0; i<mid; i++) {
			double x = (double)rand_r(&(data->seed))/RAND_MAX;
			double y = (double)rand_r(&(data->seed))/RAND_MAX;
			double dist = sqrt((x-0.5)*(x-0.5)+(y-0.5)*(y-0.5));
			if(dist<=0.5) {
				inside++;
			}	
		}
	}
	#pragma omp section
	{
		for (int i = mid; i<n-1; i++) {
			double x = (double)rand_r(&(data->seed))/RAND_MAX;
			double y = (double)rand_r(&(data->seed))/RAND_MAX;
			double dist = sqrt((x-0.5)*(x-0.5)+(y-0.5)*(y-0.5));
			if(dist<=0.5) {
				inside++;
			}	
		}
	}
	}
	data->circle = inside;
return NULL;
}


double my_pi(int n) {
	struct dart data;
	data.n = 2*n;
	data.seed = 3;
	
	circler((void*)&data);
	int sum = data.circle;
	int N = data.n;	


/*        struct dart dataa, datab;
        dataa.n = n;
        dataa.seed = 4;
        datab.n = n;
        datab.seed = 3;

        pthread_t thread_a;
        pthread_create(&thread_a,NULL,circler,(void*)&dataa);

        pthread_t thread_b;
        pthread_create(&thread_b,NULL,circler,(void*)&datab);

        pthread_join(thread_a,NULL);
        pthread_join(thread_b,NULL);

        int sum = dataa.circle+datab.circle;
        int N = dataa.n+datab.n;
*/
        double my_pi = 4 * (double)sum / N;
	
	return my_pi;
}

int main(int argc, char** argv) {
	if(argc<=1) {printf("my_pi = %g\n",my_pi(1e8));}
	if(argc==2) {printf("my_pi = %g\n",my_pi( (int)atof(argv[1]) ));}
	if(argc>2) {
		int points[argc-1];
		for(int i=0;i<argc-1;i++) {
			points[i]=atoi(argv[i+1]);
			fprintf(stdout,"%.8g %i\n",my_pi(points[i]),points[i]);
			}

	}

return 0;
}
